import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/screens/login_signup_screens/login_screen.dart';
import 'package:faen_app_user/widgets/tiles/order_tile.dart';
import "package:flutter/material.dart";

class OrdersTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /*if (UserModel.of(context).isLoggedIn()) {*/
    String uid = UserModel.of(context).firebaseUser.uid;
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "images/Fundo2.png",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Positioned.fill(
          child: Scaffold(
              backgroundColor: Colors.black87,
              appBar: AppBar(
                automaticallyImplyLeading: true,
                centerTitle: true,
                titleSpacing: 0.0,
                title: Padding(
                  padding: const EdgeInsets.all(35.0),
                  child: Hero(
                    tag: "appBar",
                    child: Image.asset("images/AppBarImage.png",
                        fit: BoxFit.contain),
                  ),
                ), //, width: 140, height: 70),
              ),
              body: UserModel.of(context).isLoggedIn()
                  ? FutureBuilder<QuerySnapshot>(
                      future: Firestore.instance
                          .collection("users")
                          .document(uid)
                          .collection("orders")
                          .getDocuments(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return Center(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.white),
                            ),
                          );
                        else if (snapshot.data.documents.length == 0)
                          return Center(
                            child: Text(
                              "Você não possuí nenhum pedido ainda : (",
                              softWrap: true,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white70, fontSize: 16),
                            ),
                          );
                        else {
                          return ListView(
                            children: snapshot.data.documents
                                .map((doc) => OrderTile(doc.documentID))
                                .toList()
                                .reversed
                                .toList(),
                          );
                        }
                      })
                  : Container(
                      padding: EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Icon(
                            Icons.view_list,
                            size: 80.0,
                            color: Theme.of(context).primaryColor,
                          ),
                          SizedBox(
                            height: 16.0,
                          ),
                          Text(
                            "Faça login para acompanhar !",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 16.0,
                          ),
                          RaisedButton(
                            child: Text(
                              "Entra",
                              style: TextStyle(fontSize: 18.0),
                            ),
                            textColor: Colors.white,
                            color: Theme.of(context).primaryColor,
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => LoginScreen()));
                            },
                          ),
                        ],
                      ),
                    )),
        ),
      ],
    );
  }
}
