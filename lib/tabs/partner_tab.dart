import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/widgets/partner_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class PartnerTab extends StatefulWidget {
  @override
  _PartnerTabState createState() => _PartnerTabState();
}

class _PartnerTabState extends State<PartnerTab> {
  @override
  Widget build(BuildContext context) {
    if (UserModel.of(context).userData["typeUser"] != "Não Associado") {
      return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("partners").getDocuments(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            );
          return AnimationLimiter(
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 4.0,
                childAspectRatio: 0.75,
              ),
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index) {
                return AnimationConfiguration.staggeredGrid(
                    position: index,
                    duration: const Duration(milliseconds: 375),
                    columnCount: 2,
                    child: ScaleAnimation(
                        child: FadeInAnimation(
                            child:
                                PartnerTile(snapshot.data.documents[index]))));
              },
            ),
          );
        },
      );
    } else {
      return Center(
        child: Text(
          "Somente membros Associados podem visualizar\na tela de parcerias : (",
          textAlign: TextAlign.center,
          softWrap: true,
          style: TextStyle(color: Colors.red[900], fontSize: 16),
        ),
      );
    }
  }
}
