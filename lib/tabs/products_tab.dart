import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/screens/category_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProductsTab extends StatelessWidget {
  //final PageController _controller;

  //ProductsTab(this._controller);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor:
          Colors.black.withOpacity(0.70), // navigation bar color
      systemNavigationBarDividerColor: Colors.black87,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor:
          Theme.of(context).primaryColor.withOpacity(0.70), // status bar color
    ));
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance.collection("products").getDocuments(),
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),
          );
        else if (snapshot.data.documents.length == 0)
          return Center(
            child: Text(
              "Não há produtos disponíveis no momento !",
              textAlign: TextAlign.center,
              softWrap: true,
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          );
        else {
          return DefaultTabController(
            length: snapshot.data.documents.length,
            child: Scaffold(
              //drawer: CustomDrawer(_controller),
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                automaticallyImplyLeading: false,
                centerTitle: true,
                title: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Image.asset("images/AppBarImage.png",
                      fit: BoxFit.contain),
                ), //, width: 140, height: 70),
                //title: Text("Loja FAEN"),
                bottom: TabBar(
                  indicatorColor: Colors.white,
                  isScrollable: true,
                  tabs: snapshot.data.documents.map((category) {
                    return Tab(
                      child: Text(category.data["title"]),
                    );
                  }).toList(),
                ),
              ),
              body: TabBarView(
                children: snapshot.data.documents.map((category) {
                  return CategoryScreen(category);
                }).toList(),
              ),
            ),
          );
        }
      },
    );
  }
}
