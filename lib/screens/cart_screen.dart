import 'package:faen_app_user/models/cart_model.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/widgets/cards/discount_card.dart';
import 'package:faen_app_user/widgets/cart_price.dart';
import 'package:faen_app_user/widgets/tiles/cart_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'login_signup_screens/login_screen.dart';
import 'order_screen.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  String _typePay = "Dinheiro";

  static const payOptions = <String>[
    'Dinheiro',
    'Cartão',
    'Transferência',
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = payOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value),
          ))
      .toList();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.70),
      appBar: AppBar(
        //elevation: 1.0,
        backgroundColor: Colors.black.withOpacity(0.75),
        centerTitle: true,
        title: Text("Meu Carrinho"),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 8.0),
            alignment: Alignment.center,
            child: ScopedModelDescendant<CartModel>(
              builder: (context, child, model) {
                int quantidade = model.products.length;
                return Text(
                  "${quantidade ?? 0} ${quantidade == 1 ? "ITEM" : "ITENS"}",
                  style: TextStyle(fontSize: 17.0),
                );
              },
            ),
          )
        ],
      ),
      body: ScopedModelDescendant<CartModel>(
        builder: (context, child, model) {
          if (model.isLoading && UserModel.of(context).isLoggedIn()) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            );
          } else if (!UserModel.of(context).isLoggedIn()) {
            return Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Icon(
                    Icons.remove_shopping_cart,
                    size: 80.0,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Text(
                    "Faça login para adicionar produtos !",
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  RaisedButton(
                    child: Text(
                      "Entrar",
                      style: TextStyle(fontSize: 18.0),
                    ),
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => LoginScreen()));
                    },
                  ),
                ],
              ),
            );
          } else if (model.products == null || model.products.length == 0) {
            return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                 crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.height * 0.24,
                    height: MediaQuery.of(context).size.height * 0.24,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("images/FormigaCarinho.png"),
                          fit: BoxFit.contain),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    "Nenhum produto no carrinho!",
                    style: TextStyle(
                      color: Colors.white70,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ]);
          } else {
            return ListView(
              children: <Widget>[
                Column(
                  children: model.products.map((product) {
                    return CartTile(product);
                  }).toList(),
                ),
                DiscountCard(),
                //ShipCard(),
                typePayTile(),
                CartPrice(() async {
                  String orderId = await model.finishOrder(_typePay);
                  if (orderId != null) {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => OrderScreen(orderId)));
                  }
                }),
              ],
            );
          }
        },
      ),
    );
  }

  Widget typePayTile() {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: ListTile(
        //contentPadding: EdgeInsets.zero,
        leading: Icon(
          Icons.credit_card,
          color: Colors.white,
        ),
        title: Text("Forma de pagamento",
            softWrap: true,
            textAlign: TextAlign.start,
            style: TextStyle(
              //fontSize: 14.0,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            )),
        trailing: DropdownButton(
          value: _typePay,
          //hint: Text("Escolha seu curso"),
          style: TextStyle(
              fontFamily: "BebasNeue", color: Colors.greenAccent[700]),
          items: _dropDownMenuItems,
          onChanged: ((newValue) {
            setState(() {
              _typePay = newValue;
            });
          }),
        ),
      ),
    );
  }
}
