import 'dart:io';

import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/widgets/image_source_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:scoped_model/scoped_model.dart';

import 'user_created_screen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _nameController = TextEditingController();
  final _dateController = MaskedTextController(mask: "00/00/0000");
  final _phoneController = MaskedTextController(mask: "(00)0 0000-0000");
  final _cpfController = MaskedTextController(mask: "000.000.000-00");
  final _rgController = TextEditingController();
  final _rgOEController = TextEditingController();
  //final _courseController = TextEditingController();

  String _course;
  int _semester;
  String _typePay;
  File image;

  final _semesterController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _passConfirmController = TextEditingController();

  bool isObscure = true;
  bool _hasError = false;
  bool isAutoValid = false;
  bool _imageValidator = true;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _nameFocus = FocusNode();
  final _dateFocus = FocusNode();
  final _cpfFocus = FocusNode();
  final _rgFocus = FocusNode();
  final _rgOEFocus = FocusNode();
  final _phoneFocus = FocusNode();
  final _emailFocus = FocusNode();
  final _passFocus = FocusNode();
  final _passConfirmFocus = FocusNode();

  static const courseOptions = <String>[
    'Agronomia',
    'Engenharia Agrícola',
    'Engenharia de Aquicultura',
    'Engenharia de Computação',
    'Engenharia de Energia',
    'Engenharia Civil',
    'Engenharia de Alimentos',
    'Engenharia Mecânica',
    'Engenharia de Produção',
    'Outros'
  ];

  static const payOptions = <String>[
    'Dinheiro',
    'Cartão',
    'Transferência',
  ];

  static const semesterOptions = <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = courseOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value, textAlign: TextAlign.center),
          ))
      .toList();

  final List<DropdownMenuItem<int>> _dropDownMenuItems2 = semesterOptions
      .map((int value) => DropdownMenuItem(
            value: value,
            child: Text("$value"),
          ))
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItems3 = payOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value),
          ))
      .toList();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => showAlertDialog(context));
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _nameController.dispose();
    _dateController.dispose();
    _cpfController.dispose();
    //_courseController.dispose();
    _semesterController.dispose();
    _phoneController.dispose();
    _passController.dispose();
    _passConfirmController.dispose();
    _rgController.dispose();
    _rgOEController.dispose();
    _emailFocus.dispose();
    _passFocus.dispose();
    _passConfirmFocus.dispose();
    _nameFocus.dispose();
    _dateFocus.dispose();
    _phoneFocus.dispose();
    _cpfFocus.dispose();
    _rgFocus.dispose();
    _rgOEFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      /*appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),*/
      backgroundColor: Colors.black,
      body: SafeArea(
        child:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          while (model.isLoading)
            return Center(
                child: Container(
              //height: 200,
              //width: 200,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
              //child: FlareActor("animations/Carregar.flr", animation: "infinito"),
            ));
          return Form(
            key: _formKey,
            child: ListView(
              //padding: EdgeInsets.all(16.0),
              children: <Widget>[
                Stack(
                  //fit: StackFit.expand,
                  children: <Widget>[
                    Image.asset(
                      "images/Fundo2.png",
                      fit: BoxFit.cover,
                      //repeat: ImageRepeat.repeatY,
                      height: MediaQuery.of(context).size.height * 2,
                      width: MediaQuery.of(context).size.width,
                    ),
                    Image.asset(
                      "images/headerSemFundo.png",
                      fit: BoxFit.cover,
                      //repeat: ImageRepeat.repeatY,
                      //height: 100,
                      width: MediaQuery.of(context).size.width,
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                      margin: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 130.0),
                      color: Colors.black.withOpacity(0.7),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            /*Align(
                              alignment: Alignment.topLeft,
                              child: FlatButton(
                                child: Text("<Voltar"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),*/
                            Center(
                                child: InkWell(
                              child: Container(
                                width: 140.0,
                                height: 140.0,
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.50),
                                  shape: BoxShape.circle,
                                  image: image != null
                                      ? DecorationImage(
                                          image: FileImage(image),
                                          fit: BoxFit.cover)
                                      : null,
                                ),
                                child: image == null
                                    ? Icon(
                                        Icons.add_a_photo,
                                        color: _imageValidator
                                            ? Colors.white70
                                            : Colors.red[900],
                                        size: 88.0,
                                      )
                                    : null,
                              ),
                              //AssetImage("images/person.png")
                              onTap: () {
                                showModalBottomSheet(
                                    context: context,
                                    builder: (context) => ImageSourceSheet(
                                          onImageSelected: (image) {
                                            setState(() {
                                              this.image = image;
                                              _imageValidator = true;
                                            });
                                            Navigator.of(context).pop();
                                          },
                                        ));
                              },
                            )),
                            SizedBox(height: 3.0),
                            Text(
                              image == null
                                  ? "Insira sua foto"
                                  : "Essa ficou perfeita !",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white70, fontSize: 16.0),
                            ),
                            SizedBox(height: 16.0),
                            _inputFormField(
                                label: "Nome Completo",
                                controller: _nameController,
                                textValidator: "Insira seu nome !",
                                focus: _nameFocus,
                                showCursor: true,
                                typeInput: TextInputType.text),

                            SizedBox(height: 16.0),
                            _inputFormField(
                                label: "Data de Nascimento",
                                controller: _dateController,
                                focus: _dateFocus,
                                showCursor: false,
                                validate: Theme.of(context).platform ==
                                        TargetPlatform.iOS
                                    ? false
                                    : true,
                                textValidator:
                                    "Insira sua data de nascimento !",
                                hintText: "23/09/2000",
                                typeInput: TextInputType.datetime),
                            SizedBox(height: 16.0),
                            //Text("Digite seu CPF"),
                            _inputFormField(
                                label: "CPF",
                                controller: _cpfController,
                                hintText: "123.456.789-00",
                                validate: Theme.of(context).platform ==
                                        TargetPlatform.iOS
                                    ? false
                                    : true,
                                textValidator: "Insira seu CPF !",
                                typeInput: TextInputType.number,
                                focus: _cpfFocus,
                                showCursor: false),

                            SizedBox(height: 16.0),
                            _inputFormField(
                                label: "Telefone",
                                controller: _phoneController,
                                hintText: "(67)9 9999-9999",
                                validate: Theme.of(context).platform ==
                                        TargetPlatform.iOS
                                    ? false
                                    : true,
                                textValidator:
                                    "Insira seu número de telefone !",
                                typeInput: TextInputType.phone,
                                focus: _phoneFocus,
                                showCursor: false),
                            SizedBox(height: 16.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                SizedBox(
                                  //height: 150,
                                  width:
                                      MediaQuery.of(context).size.width * 0.45,
                                  child: _inputFormField(
                                      label: "RG",
                                      controller: _rgController,
                                      focus: _rgFocus,
                                      validate: Theme.of(context).platform ==
                                              TargetPlatform.iOS
                                          ? false
                                          : true,
                                      textValidator:
                                          "Insira o número do seu RG",
                                      typeInput:
                                          TextInputType.numberWithOptions(),
                                      showCursor: true),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.30,
                                  child: _inputFormField(
                                      label: "Orgão\nExpedidor",
                                      controller: _rgOEController,
                                      focus: _rgOEFocus,
                                      showCursor: true,
                                      validate: Theme.of(context).platform ==
                                              TargetPlatform.iOS
                                          ? false
                                          : true,
                                      textValidator:
                                          "É necessário inserir o Orgão Expedidor"),
                                ),
                              ],
                            ),
                            SizedBox(height: 16.0),
                            ListTile(
                              dense: true,
                              //contentPadding: EdgeInsets.zero,
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 2.0),
                              title: Text(
                                "Defina seu curso:",
                                style: TextStyle(color: Colors.white70),
                                softWrap: true,
                              ),
                              trailing: Card(
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                margin: EdgeInsets.zero,
                                color: Colors.white38,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 2.0, left: 2.0),
                                  child: DropdownButton(
                                    value: _course,
                                    hint: Text("Escolha seu curso"),
                                    /*style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                    ),*/
                                    items: _dropDownMenuItems,
                                    onChanged: ((newValue) {
                                      setState(() {
                                        _course = newValue;
                                      });
                                    }),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16.0),
                            ListTile(
                              contentPadding: EdgeInsets.zero,
                              title: Text(
                                "Escolha seu semestre atual:",
                                style: TextStyle(color: Colors.white70),
                                softWrap: true,
                              ),
                              trailing: Card(
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                margin: EdgeInsets.zero,
                                color: Colors.white38,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 2.0, left: 2.0),
                                  child: DropdownButton(
                                    value: _semester,
                                    //hint: Text("Escolha seu curso"),
                                    items: _dropDownMenuItems2,
                                    onChanged: ((newValue) {
                                      setState(() {
                                        _semester = newValue;
                                      });
                                    }),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16.0),
                            ListTile(
                              contentPadding: EdgeInsets.zero,
                              title: Text(
                                "Escolha uma forma de pagamento:",
                                softWrap: true,
                                style: TextStyle(color: Colors.white70),
                              ),
                              trailing: Card(
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                margin: EdgeInsets.zero,
                                color: Colors.white38,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 2.0, left: 2.0),
                                  child: DropdownButton(
                                    value: _typePay,
                                    //hint: Text("Escolha seu curso"),
                                    items: _dropDownMenuItems3,
                                    onChanged: ((newValue) {
                                      setState(() {
                                        _typePay = newValue;
                                      });
                                    }),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16.0),
                            TextFormField(
                              controller: _emailController,
                              focusNode: _emailFocus,
                              style: TextStyle(
                                fontFamily: 'Lato',
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white38,
                                labelText: "Email",
                                labelStyle: TextStyle(color: Colors.white70),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white60,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.green[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                              ),
                              keyboardType: TextInputType.emailAddress,
                              onFieldSubmitted: (text) {
                                return FocusScope.of(context)
                                    .requestFocus(_passFocus);
                              },
                              validator: (text) {
                                if (text.isEmpty || !text.contains("@")) {
                                  FocusScope.of(context)
                                      .requestFocus(_emailFocus);
                                  return "Email inválido !";
                                } else
                                  return null;
                              },
                            ),
                            SizedBox(height: 16.0),
                            TextFormField(
                              controller: _passController,
                              focusNode: _passFocus,
                              style: TextStyle(
                                fontFamily: 'Lato',
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white38,
                                suffixIcon: IconButton(
                                  icon: isObscure
                                      ? Icon(Icons.visibility_off)
                                      : Icon(Icons.visibility),
                                  onPressed: () {
                                    setState(() {
                                      isObscure = !isObscure;
                                    });
                                  },
                                ),
                                labelText: "Senha",
                                labelStyle: TextStyle(color: Colors.white70),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white60,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.green[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                              ),
                              obscureText: isObscure,
                              onFieldSubmitted: (text) {
                                setState(() {
                                  isAutoValid = true;
                                });
                                return FocusScope.of(context)
                                    .requestFocus(_passConfirmFocus);
                              },
                              validator: (text) {
                                if (text.isEmpty || text.length < 6) {
                                  FocusScope.of(context)
                                      .requestFocus(_passFocus);
                                  return "Senha inválida !";
                                } else
                                  return null;
                              },
                            ),
                            SizedBox(height: 16.0),
                            TextFormField(
                              controller: _passConfirmController,
                              focusNode: _passConfirmFocus,
                              autovalidate: isAutoValid,
                              style: TextStyle(
                                fontFamily: 'Lato',
                              ),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white38,
                                suffixIcon: IconButton(
                                  icon: isObscure
                                      ? Icon(Icons.visibility_off)
                                      : Icon(Icons.visibility),
                                  onPressed: () {
                                    setState(() {
                                      isObscure = !isObscure;
                                    });
                                  },
                                ),
                                labelText: "Confirme sua senha",
                                labelStyle: TextStyle(color: Colors.white70),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.green[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red[900],
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                              ),
                              obscureText: isObscure,
                              validator: (text) {
                                if (_passController.text != text) {
                                  FocusScope.of(context)
                                      .requestFocus(_passConfirmFocus);
                                  return "O valor digitado não é igual ao anterior";
                                } else
                                  return null;
                              },
                            ),
                            SizedBox(height: 16.0),
                            Visibility(
                              visible: !_imageValidator,
                              child: Text(
                                "Por favor, volte ao início do formulário e adicione uma foto de perfil !",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.red[900],
                                ),
                                softWrap: true,
                              ),
                            ),
                            SizedBox(height: 16.0),
                            SizedBox(
                              height: 44.0,
                              width: MediaQuery.of(context).size.width * 0.5,
                              child: RaisedButton(
                                padding: EdgeInsets.zero,
                                shape: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: !_hasError
                                          ? Color.fromARGB(122, 4, 64, 0)
                                          : Colors.red[900]),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12.5)),
                                ),
                                child: Text(
                                  "Criar Conta",
                                  style: TextStyle(fontSize: 18.0),
                                ),
                                textColor: Theme.of(context).primaryColor,
                                color: Colors.white,
                                onPressed: () {
                                  /* Navigator.of(context)
                                      .pushReplacement(MaterialPageRoute(
                                    builder: (context) => UserCreatedScreen(),
                                  ));*/
                                  if (_formKey.currentState.validate() &&
                                      image != null) {
                                    Map<String, dynamic> userData = {
                                      "name": _nameController.text,
                                      "photo": image,
                                      "cpf": _cpfController.text,
                                      "rg": _rgController.text,
                                      "rgOE": _rgOEController.text,
                                      "email": _emailController.text,
                                      "date": _dateController.text,
                                      "phone": _phoneController.text,
                                      "course": _course,
                                      "semester": _semester,
                                      "typeUser": "Não Associado",
                                      "typePay": _typePay,
                                      "allDocumentReceived": false,

                                      //adicionar outras informações necessárias
                                    };
                                    model.signUp(
                                        userData: userData,
                                        pass: _passController.text,
                                        onSuccess: _onSuccess,
                                        onFail: _onFail);
                                  } else {
                                    _imageValidator =
                                        image == null ? false : true;
                                    _hasError = true;
                                  }
                                  setState(() {});
                                },
                              ),
                            ),
                            //_errorCard(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget _inputFormField(
      {@required String label,
      @required TextEditingController controller,
      bool validate,
      String textValidator,
      String hintText,
      @required bool showCursor,
      TextInputType typeInput,
      @required FocusNode focus}) {
    validate = validate ?? true;
    return TextFormField(
      enableSuggestions: true,
      showCursor: showCursor,
      focusNode: focus,
      keyboardType: typeInput,
      controller: controller,
      style: TextStyle(
        fontFamily: 'Lato',
      ),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white38,
        labelText: label,
        labelStyle: TextStyle(color: Colors.white70),
        hintText: hintText ?? "",
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.green[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
      ),
      validator: (text) {
        if (text.isEmpty && validate) {
          FocusScope.of(context).requestFocus(focus);
          return textValidator;
        } else
          return null;
      },
    );
  }

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Entendi!",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(
              "Avisos importantes!",
              textAlign: TextAlign.center,
            ),
            content: Text(
              "- Nenhum campo do formulário poderá ser deixado em branco.\n"
              "- Certifique-se de preencher todos os dados pessoais corretamente.\n"
              "- Após criar sua conta, bastará enviar a cópia do seus documentos, e efetuar o pagamento para se tornar um membro Associado FAEN.",
              textAlign: TextAlign.justify,
            ),
            actions: [botaoContinuar]);
      },
    );
  }

  /*Widget _errorCard() {
    return Visibility(
      visible: _hasError,
      child: Container(
        color: Colors.white54,
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red[900]),
          borderRadius: BorderRadius.all(Radius.circular(12.5)),
        ),
        child: Text(
            "Há erros no preenchimento do formulário, verifique se nenhum campo foi deixado em branco !",
            textAlign: TextAlign.center,
            softWrap: true,
            style: TextStyle(color: Colors.red[900], fontSize: 16.0)),
      ),
    );
  }*/

  void _onSuccess() {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text("Usuário criado com sucesso !"),
        backgroundColor: Theme.of(context).primaryColor,
        duration: Duration(seconds: 2),
      ),
    );
    Future.delayed(Duration(seconds: 2)).then((_) {
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => UserCreatedScreen(),
      ));
    });
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao criar o usuário !"),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 2),
    ));
  }
}

/*Center(
                                child: InkWell(
                              child: Container(
                                width: 140.0,
                                height: 140.0,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  shape: BoxShape.circle,
                                  image: image != null ? DecorationImage(
                                      image: image != null
                                          ? FileImage(image)
                                          : Icon(
                                              Icons.add_a_photo,
                                              size: 88.0,
                                            ),
                                      fit: BoxFit.cover),
                                ),
                              ),
                              //AssetImage("images/person.png")
                              onTap: () {
                                showModalBottomSheet(
                                    context: context,
                                    builder: (context) => ImageSourceSheet(
                                          onImageSelected: (image) {
                                            setState(() {
                                              this.image = image;
                                            });
                                            Navigator.of(context).pop();
                                          },
                                        ));
                              },
                            )),*/
