import 'package:flutter/material.dart';
import '../../SizeConfig.dart';
import '../home_screen.dart';

class UserCreatedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          backgroundColor: Theme.of(context).primaryColor,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.green[600],
            child: Icon(Icons.arrow_forward_ios),
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          body: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Image.asset(
                    "images/Fundo2.png",
                    fit: BoxFit.cover,
                    height: SizeConfig.safeBlockVertical * 100,
                    //height: MediaQuery.of(context).size.height,
                    width: SizeConfig.safeBlockHorizontal * 100,
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.center,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.0,
                            vertical:
                                MediaQuery.of(context).size.height * 0.28),
                        color: Colors.black.withOpacity(0.7),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                width: 140.0,
                                height: 140.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage("images/logo.png"),
                                      fit: BoxFit.cover),
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text:
                                        "Sua conta foi criada com sucesso !\n",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text:
                                              "Agora basta efetuar o pagamento da sua carteirinha para se tornar um ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                              fontSize: 16)),
                                      TextSpan(
                                          text: "Membro Associado FAEN ",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white)),
                                      TextSpan(
                                        text: ": )",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ]),
                              ),
                              /*Container(
                              width: 140.0,
                              height: 140.0,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                      image: AssetImage("images/logo.png"),
                                      fit: BoxFit.cover),
                              ),
                            ),*/
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
