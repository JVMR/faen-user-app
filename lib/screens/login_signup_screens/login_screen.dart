import 'package:faen_app_user/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../SizeConfig.dart';
import '../home_screen.dart';
import 'signup_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isObscure = true;
  bool isOpenAssociations;

  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _emailFocus = FocusNode();
  final _passFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    // Future.delayed(Duration(seconds: 3), () {});
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passController.dispose();
    _emailFocus.dispose();
    _passFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor:
          Colors.black.withOpacity(0.70), // navigation bar color
      systemNavigationBarDividerColor: Colors.black87,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor:
          Theme.of(context).primaryColor.withOpacity(0.7), // status bar color
    ));
    SizeConfig().init(context);
    return WillPopScope(
      //Fecha o app caso o retorno seja solicitado
      onWillPop: () {
        return SystemChannels.platform
                .invokeMethod<void>('SystemNavigator.pop', true) ??
            false;
      },
      child: Scaffold(
          backgroundColor: Colors.black,
          key: _scaffoldKey,
          body: SafeArea(
            child: ScopedModelDescendant<UserModel>(
                builder: (context, child, model) {
              void signIn() {
                model.signIn(
                    email: _emailController.text,
                    pass: _passController.text,
                    onSuccess: _onSuccess,
                    onFail: _onFail);
              }

              while (model.isLoading)
                return Center(
                    child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                ));

              /* if (isOpenAssociations == null) {
                model.associationIsOpen().then((value) {
                  setState(() {
                    isOpenAssociations = value;
                  });
                });
              }*/
              model.associationIsOpen();
              return Form(
                key: _formKey,
                child: ListView(
                  //padding: EdgeInsets.zero,
                  children: <Widget>[
                    Stack(
                      // fit: StackFit.expand,
                      children: <Widget>[
                        Image.asset(
                          "images/Fundo2.png",
                          fit: BoxFit.cover,
                          height: SizeConfig.safeBlockVertical * 100,
                          width: SizeConfig.safeBlockHorizontal * 100,
                        ),
                        Positioned.fill(
                          child: Align(
                            alignment: Alignment.center,
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 60.0),
                              color: Colors.black.withOpacity(0.7),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Container(
                                      width: 140.0,
                                      height: 140.0,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image:
                                                AssetImage("images/logo.png"),
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    SizedBox(height: 16.0),
                                    TextFormField(
                                      focusNode: _emailFocus,
                                      autocorrect: true,
                                      enableSuggestions: true,
                                      controller: _emailController,
                                      style: TextStyle(
                                          fontFamily: 'Lato',
                                        color: Colors.white70),
                                      decoration: InputDecoration(
                                        hoverColor: Colors.white70,
                                        labelText: "Email",
                                        labelStyle:
                                            TextStyle(color: Colors.white70),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.white60,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.green[900],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                        focusedErrorBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.red[900],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                        errorBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.red[900],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                      onFieldSubmitted: (_) {
                                        FocusScope.of(context)
                                            .requestFocus(_passFocus);
                                      },
                                      validator: (text) {
                                        if (text.isEmpty ||
                                            !text.contains("@")) {
                                          FocusScope.of(context)
                                              .requestFocus(_emailFocus);
                                          return "Email inválido !";
                                        } else
                                          return null;
                                      },
                                    ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: FlatButton(
                                        child: Text(
                                          "Criar Conta",
                                          //textAlign: TextAlign.right,
                                          style:
                                              TextStyle(color: Colors.white70),
                                        ),
                                        padding: EdgeInsets.zero,
                                        onPressed: model.assIsOpen != null &&
                                                model.assIsOpen == true
                                            ? () {
                                                print(model.assIsOpen);
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            SignUpScreen()));
                                              }
                                            : () {
                                                print(model.assIsOpen);
                                                showAlertDialog(
                                                    context, model.assIsOpen);
                                              },
                                        /* isOpenAssociations !=
                                              null &&
                                          isOpenAssociations == true
                                      ? () {
                                          print(isOpenAssociations);
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                    builder: (context) =>
                                                        SignUpScreen()));
                                        }
                                      : () {
                                          print(isOpenAssociations);
                                          showAlertDialog(context);
                                        },*/
                                      ),
                                    ),
                                    SizedBox(height: 10.0),
                                    TextFormField(
                                      controller: _passController,
                                      style: TextStyle(
                                          fontFamily: 'Lato',
                                        color: Colors.white70),
                                      decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          icon: isObscure
                                              ? Icon(Icons.visibility,
                                                  color: Colors.white54)
                                              : Icon(Icons.visibility_off,
                                                  color: Colors.white54),
                                          onPressed: () {
                                            setState(() {
                                              isObscure = !isObscure;
                                            });
                                          },
                                        ),
                                        labelText: "Senha",
                                        labelStyle:
                                            TextStyle(color: Colors.white70),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.white60,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.green[900],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                        focusedErrorBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.red[900],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                        errorBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.red[900],
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(12.5)),
                                      ),
                                      obscureText: isObscure,
                                      onFieldSubmitted: (_) {
                                        if (_formKey.currentState.validate()) {
                                          signIn();
                                        }
                                      },
                                      validator: (text) {
                                        if (text.isEmpty || text.length < 6)
                                          return "Senha inválida !";
                                        else
                                          return null;
                                      },
                                    ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: FlatButton(
                                        onPressed: () {
                                          if (_emailController.text.isEmpty) {
                                            _scaffoldKey.currentState
                                                .showSnackBar(SnackBar(
                                              content: Text(
                                                  "Insira seu email para recuperação"),
                                              backgroundColor: Colors.redAccent,
                                              duration: Duration(seconds: 2),
                                            ));
                                          } else {
                                            model.recoverPass(
                                                _emailController.text);
                                            _scaffoldKey.currentState
                                                .showSnackBar(SnackBar(
                                              content:
                                                  Text("Confira seu email !"),
                                              backgroundColor: Theme.of(context)
                                                  .primaryColor,
                                              duration: Duration(seconds: 2),
                                            ));
                                          }
                                        },
                                        child: Text(
                                          "Esqueci minha senha",
                                          textAlign: TextAlign.right,
                                          style:
                                              TextStyle(color: Colors.white70),
                                        ),
                                        padding: EdgeInsets.zero,
                                      ),
                                    ),
                                    SizedBox(height: 16.0),
                                    SizedBox(
                                      height: 44.0,
                                      width: MediaQuery.of(context).size.width *
                                          0.7,
                                      child: RaisedButton(
                                        shape: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color.fromARGB(
                                                  122, 4, 64, 0)),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12.5)),
                                        ),
                                        child: Text(
                                          "Entrar",
                                          style: TextStyle(fontSize: 18.0),
                                        ),
                                        textColor:
                                            Theme.of(context).primaryColor,
                                        color: Colors.white,
                                        onPressed: () {
                                          if (_formKey.currentState
                                              .validate()) {
                                            signIn();
                                          }
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 16.0),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              );
            }),
          )),
    );
  }

  void _onSuccess() {
    // Navigator.of(context).pop();
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => HomeScreen()));
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao entrar !"),
      backgroundColor: Colors.red[900],
      duration: Duration(seconds: 2),
    ));
  }

  showAlertDialog(BuildContext context, bool result) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Entendi!",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    String title = result != null
        ? "Associações encerradas!"
        : "Não foi possível verificar se as associações estão abertas !";
    String body = result != null
        ? "O período de associações foi encerrado, logo, não será possível criar novas contas.\n"
            "Contate a Diretoria AAAFAEN para mais informações"
        : "Verifique sua conexão com a internet, e/ou tente novamente mais tarde.";
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(
              title,
              textAlign: TextAlign.center,
            ),
            content: Text(
              body,
              textAlign: TextAlign.justify,
            ),
            actions: [botaoContinuar]);
      },
    );
  }
}
