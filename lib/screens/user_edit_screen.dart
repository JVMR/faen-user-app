import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/widgets/image_source_sheet.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:scoped_model/scoped_model.dart';

import '../SizeConfig.dart';
import 'home_screen.dart';

class UserEditScreen extends StatefulWidget {
  final Map<String, dynamic> userData;
  UserEditScreen(this.userData);
  @override
  _UserEditScreenState createState() => _UserEditScreenState(userData);
}

class _UserEditScreenState extends State<UserEditScreen> {
  Map<String, dynamic> userData;
  _UserEditScreenState(this.userData);

  @override
  void initState() {
    super.initState();
    _course = userData["course"];
    _phoneController.text = userData["phone"];
    _emailController.text = userData["email"];
  }

  final _phoneController = MaskedTextController(mask: "(00)0 0000-0000");
  final _emailController = TextEditingController();
  String _course;
  File image;

  bool isObscure = true;
  bool _hasError = false;
  bool isAutoValid = false;
  bool updateEmailSucess;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _phoneFocus = FocusNode();
  final _emailFocus = FocusNode();

  static const courseOptions = <String>[
    'Agronomia',
    'Engenharia Agrícola',
    'Engenharia de Aquicultura',
    'Engenharia de Computação',
    'Engenharia de Energia',
    'Engenharia Civil',
    'Engenharia de Alimentos',
    'Engenharia Mecânica',
    'Engenharia de Produção'
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = courseOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value, textAlign: TextAlign.center),
          ))
      .toList();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    _phoneFocus.dispose();
    _emailFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          // title: Text("Editar Perfil"),
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent,
        body:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          void _updateUserData() async {
            if (userData["email"] != _emailController.text) {
              updateEmailSucess =
                  await model.updateEmail(_emailController.text);
              if (updateEmailSucess ?? false) {
                userData["email"] = _emailController.text;
              }
            }
            if (image != null) {
              // userData["name"] = model.userData["name"];
              userData["photo"] = image;
              userData["phone"] = _phoneController.text;
              userData["couse"] = _course;
            } else {
              //userData["name"] = model.userData["name"];
              userData["phone"] = _phoneController.text;
              // userData["image"] = model.userData["photo"];
              userData["couse"] = _course;
            }
            bool sucess = await model.updateUserData(userData);
            if (sucess) {
              _onSuccess(updateEmailSucess ?? true);
            } else {
              _onFail(updateEmailSucess ?? false);
            }
          }

          while (model.isLoading)
            return Center(
                child: Container(
              //height: 200,
              //width: 200,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
              //child: FlareActor("animations/Carregar.flr", animation: "infinito"),
            ));
          return Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  ),
                  margin:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                  // color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Center(
                            child: InkWell(
                          child: Container(
                            width: 140.0,
                            height: 140.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: image != null
                                      ? FileImage(image)
                                      : NetworkImage(userData["photo"]),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (context) => ImageSourceSheet(
                                      onImageSelected: (image) {
                                        setState(() {
                                          this.image = image;
                                        });
                                        Navigator.of(context).pop();
                                      },
                                    ));
                          },
                        )),
                        SizedBox(height: 3.0),
                        Text(
                          image == null
                              ? "Clique na foto para substítui-la"
                              : "Essa ficou perfeita !",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white70),
                        ),
                        SizedBox(height: 16.0),
                        TextFormField(
                          focusNode: _emailFocus,
                          autocorrect: true,
                          enableSuggestions: true,
                          controller: _emailController,
                          style: TextStyle(
                            fontFamily: 'Lato',
                          ),
                          // style: TextStyle(color: Colors.white70),
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white38,
                            hoverColor: Colors.white70,
                            labelText: "Email",
                            labelStyle: TextStyle(color: Colors.white70),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white60,
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Theme.of(context).primaryColor,
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          validator: (text) {
                            if (text.isEmpty || !text.contains("@")) {
                              FocusScope.of(context).requestFocus(_emailFocus);
                              return "Email inválido !";
                            } else
                              return null;
                          },
                        ),
                        SizedBox(height: 16.0),
                        _inputFormField(
                            label: "Telefone",
                            controller: _phoneController,
                            hintText: "(67)9 9999-9999",
                            textValidator: "Insira seu número de telefone !",
                            typeInput: TextInputType.phone,
                            focus: _phoneFocus,
                            showCursor: false),
                        SizedBox(height: 16.0),
                        ListTile(
                          dense: true,
                          //contentPadding: EdgeInsets.zero,
                          contentPadding: EdgeInsets.symmetric(horizontal: 2.0),
                          title: Text(
                            "Defina seu curso:",
                            style: TextStyle(color: Colors.white70),
                            softWrap: true,
                          ),
                          trailing: Card(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white54,
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            margin: EdgeInsets.zero,
                            color: Colors.white38,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 2.0, left: 2.0),
                              child: DropdownButton(
                                value: _course,
                                hint: Text("Escolha seu curso"),
                                /*style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            ),*/
                                items: _dropDownMenuItems,
                                onChanged: ((newValue) {
                                  setState(() {
                                    _course = newValue;
                                  });
                                }),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 16.0),
                        Align(
                          alignment: Alignment.topLeft,
                          child: FlatButton(
                            onPressed: () {
                              model.recoverPass(userData["email"]);
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text(
                                    "Enviamos uma mensagem de recuperação de senha em seu email !"),
                                backgroundColor: Theme.of(context).primaryColor,
                                duration: Duration(seconds: 4),
                              ));
                            },
                            child: Text(
                              "Redefinir senha",
                              textAlign: TextAlign.left,
                              style: TextStyle(color: Colors.white70),
                            ),
                            padding: EdgeInsets.zero,
                          ),
                        ),
                        SizedBox(
                          height: 44.0,
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: RaisedButton(
                            padding: EdgeInsets.zero,
                            shape: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: !_hasError
                                      ? Color.fromARGB(122, 4, 64, 0)
                                      : Colors.red[900]),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12.5)),
                            ),
                            child: Text(
                              "Salvar",
                              style: TextStyle(fontSize: 18.0),
                            ),
                            textColor: Theme.of(context).primaryColor,
                            color: Colors.white,
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                _updateUserData();
                                // Navigator.of(context).pop();
                              } else {
                                _hasError = true;
                              }

                              setState(() {});
                            },
                          ),
                        ),
                        //_errorCard(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget _inputFormField(
      {@required String label,
      @required TextEditingController controller,
      String textValidator,
      String hintText,
      @required bool showCursor,
      TextInputType typeInput,
      @required FocusNode focus}) {
    return TextFormField(
      enableSuggestions: true,
      showCursor: showCursor,
      focusNode: focus,
      keyboardType: typeInput,
      controller: controller,
      style: TextStyle(
        fontFamily: 'Lato',
      ),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white38,
        labelText: label,
        labelStyle: TextStyle(color: Colors.white70),
        hintText: hintText ?? "",
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.green[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
      ),
      validator: (text) {
        if (text.isEmpty) {
          FocusScope.of(context).requestFocus(focus);
          return textValidator;
        } else
          return null;
      },
    );
  }

  void _onSuccess(bool emailResult) {
    String emailMessage = emailResult
        ? "Email atualizado com sucesso !"
        : "Falha ao atualizar o email";
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text("Informações salvas com sucesso !\n" + emailMessage),
        backgroundColor: Colors.greenAccent[700].withOpacity(0.54),
        duration: Duration(seconds: 2),
      ),
    );
    Future.delayed(Duration(seconds: 2)).then((_) {
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
    });
  }

  void _onFail(bool emailResult) {
    String emailMessage = emailResult
        ? "Email atualizado com sucesso !"
        : "Falha ao atualizar o email";
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao salvar as  informações !\n" + emailMessage),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 2),
    ));
  }
}
