import 'package:faen_app_user/datas/feed_data.dart';
import 'package:faen_app_user/widgets/markdown_modifiable.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class ViewPostScreen extends StatefulWidget {
  final FeedData data;

  ViewPostScreen(this.data);

  @override
  _ViewPostScreenState createState() => _ViewPostScreenState();
}

class _ViewPostScreenState extends State<ViewPostScreen> {
  // final controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: SafeArea(
          child: CustomScrollView(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            slivers: <Widget>[
              SliverAppBar(
                stretch: true,
                //floating: true,
                /* title: SelectableText.rich(
                  TextSpan(
                    text: widget.data.title + "\n",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 16.0),
                    /* children: <TextSpan>[
                        TextSpan(
                            text: widget.data.subTitle, // + "\n",
                            style: TextStyle(
                                height: 1.4,
                                fontWeight: FontWeight.w600,
                                color: Colors.white70,
                                fontSize: 16.0)),
                      ],*/
                  ),
                  // textAlign: TextAlign.start,
                ),*/
                centerTitle: true,
                expandedHeight: (MediaQuery.of(context).size.width * 0.98) / 2,
                flexibleSpace: FlexibleSpaceBar(
                  //centerTitle: true,
                  //titlePadding: EdgeInsets.zero,
                  collapseMode: CollapseMode.parallax,
                  stretchModes: [StretchMode.zoomBackground],
                  background: Hero(
                    tag: widget.data.image,
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: widget.data.image,
                      fit: BoxFit.cover,
                      // height: (MediaQuery.of(context).size.width * 0.98) / 2,
                      //width: MediaQuery.of(context).size.width * 0.98
                    ),
                  ),
                ),
              ),
              /*SliverFixedExtentList(
                itemExtent: 150.0,
                delegate: SliverChildListDelegate(
                  [
                    _buildMarkdown(widget.data.description ?? "", context),
                  ],
                ),
              ),*/
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SelectableText.rich(
                        TextSpan(
                          text: widget.data.title + "\n",
                          style: TextStyle(
                              //fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 18.0),
                          children: <TextSpan>[
                            TextSpan(
                                text: widget.data.subTitle, // + "\n",
                                style: TextStyle(
                                    height: 1.4,
                                    //fontWeight: FontWeight.w600,
                                    color: Colors.white70,
                                    fontSize: 18.0)),
                          ],
                        ),
                        // textAlign: TextAlign.start,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: _buildMarkdown(
                          widget.data.description ?? "", context),
                    ),
                  ],
                ),
              ),
              /*  SliverFillViewport(
                viewportFraction: 2.0,
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    return _buildMarkdown(
                        widget.data.description ?? "", context);
                  },
                  childCount: 1,
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMarkdown(String data, BuildContext context) {
    return MarkdownModifiableSelectableJustify(
      //shrinkWrap: true,
      //selectable: true,
      data: data,
      styleSheet: MarkdownStyleSheet(
        checkbox: TextStyle(color: Colors.white),
        p: Theme.of(context).textTheme.bodyText1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h1: Theme.of(context).textTheme.headline1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h2: Theme.of(context).textTheme.headline2.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h3: Theme.of(context).textTheme.headline3.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h4: Theme.of(context).textTheme.headline4.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h5: Theme.of(context).textTheme.headline5.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h6: Theme.of(context).textTheme.headline6.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
      ),

      //styleSheetTheme: MarkdownStyle(),
      /*imageDirectory:
                                  'https://raw.githubusercontent.com',*/
    );
  }
}

/*Theme(
      data: Theme.of(context).copyWith(
          canvasColor: Color.fromARGB(255, 4, 64, 0),
          primaryColor: Colors.white,
          textTheme: Theme.of(context).textTheme.copyWith(
              display2: TextStyle(
                color: Colors.white70,
              ),
              display3: TextStyle(
                color: Colors.white70,
              ),
              headline: TextStyle(
                color: Colors.white70,
              ),
              overline: TextStyle(
                color: Colors.white70,
              ),
              body1: TextStyle(
                color: Colors.white70,
              ),
              body2: TextStyle(
                color: Colors.white70,
              ),
              display1: TextStyle(
                color: Colors.white70,
              ),
              subtitle: TextStyle(
                color: Colors.white70,
              ),
              title: TextStyle(
                color: Colors.white70,
              ),
              caption: TextStyle(color: Colors.white70))),
      child: MarkdownBody(
        //controller: controller,

        shrinkWrap: true,
        selectable: true,
        data: data,
        styleSheet: MarkdownStyleSheet(
          p: Theme.of(context).textTheme.body1.copyWith(
            color: Colors.white70
          ),
          h1: Theme.of(context).textTheme.headline.copyWith(
             color: Colors.white70,
          ),
          h2: Theme.of(context).textTheme.title.copyWith(
             color: Colors.white70,
          ),
          h3: Theme.of(context).textTheme.subhead.copyWith(
             color: Colors.white70,
          ),
          h4: Theme.of(context).textTheme.body2.copyWith(
             color: Colors.white70,
          ),
          h5: Theme.of(context).textTheme.body2.copyWith(
             color: Colors.white70,
          ),
          h6: Theme.of(context).textTheme.body2.copyWith(
             color: Colors.white70,
          ),
        ),
        //styleSheetTheme: MarkdownStyle(),
        /*imageDirectory:
                                    'https://raw.githubusercontent.com',*/
      ),
    );
  }*/

/* @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(backgroundColor: Colors.black),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Image.asset(
            "images/Fundo.png",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
          Positioned(
            top: 25.0,
            left: 5.0,
            child: FlatButton.icon(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
                label: Text(
                  "Voltar",
                  style: TextStyle(color: Colors.white70),
                )),
          ),
          Positioned.fill(
            top: 70,
            child: SingleChildScrollView(
              padding: EdgeInsets.all(10.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                PhotoViewScreen(widget.data.image)));
                      },
                      child: Hero(
                        tag: widget.data.image,
                        child: FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            image: widget.data.image,
                            fit: BoxFit.cover,
                            height:
                                (MediaQuery.of(context).size.width * 0.98) / 2,
                            width: MediaQuery.of(context).size.width * 0.98),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Hero(
                      tag: widget.data.title,
                      child: SelectableText.rich(
                        TextSpan(
                          text: widget.data.title + "\n",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 18.0),
                          children: <TextSpan>[
                            TextSpan(
                                text: widget.data.subTitle, // + "\n",
                                style: TextStyle(
                                    height: 1.4,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white70,
                                    fontSize: 16.0)),
                          ],
                        ),
                        // textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    _buildMarkdown(widget.data.description ?? "", context),
                  ]),
            ),
          ),
        ],
      ),
    );
  }*/
