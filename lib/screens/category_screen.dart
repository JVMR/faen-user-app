import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/datas/products_data.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/widgets/tiles/product_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class CategoryScreen extends StatelessWidget {
  final DocumentSnapshot snapshot;

  CategoryScreen(this.snapshot);

  @override
  Widget build(BuildContext context) {
    //_enableRotation();
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance
          .collection("products")
          .document(snapshot.documentID)
          .collection("items")
          .getDocuments(),
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),
          );
        else if (snapshot.data.documents.length == 0) {
          return Center(
            child: Text(
              "Ainda não há produtos disponíveis nessa Categoria !",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          );
        } else
          return MediaQuery.of(context).orientation == Orientation.portrait
              ? AnimationLimiter(
                  child: ListView.builder(
                    padding: EdgeInsets.all(4.0),
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) {
                      ProductData data = ProductData.fromDocument(
                          snapshot.data.documents[index],
                          UserModel.of(context).userData["typeUser"]);
                      data.category = this.snapshot.documentID;
                      return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          child: SlideAnimation(
                              child: FadeInAnimation(
                                  child: ProductTile("list", data))));
                    },
                  ),
                )
              : GridView.builder(
                  padding: EdgeInsets.all(4.0),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 4.0,
                    mainAxisSpacing: 4.0,
                    childAspectRatio: 0.65,
                  ),
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    ProductData data = ProductData.fromDocument(
                        snapshot.data.documents[index],
                        UserModel.of(context).userData["typeUser"]);
                    data.category = this.snapshot.documentID;
                    return ProductTile("grid", data);
                  },
                );
      },
    );
  }

  /*void _enableRotation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }*/
}
