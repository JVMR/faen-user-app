import 'dart:io';

import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/tabs/orders_tab.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import "package:flutter/material.dart";
import 'package:scoped_model/scoped_model.dart';
import 'home_screen.dart';
import 'login_signup_screens/login_screen.dart';

class WelcomeScreen extends StatefulWidget {
  // final bool isLogged;
  WelcomeScreen();
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  //FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  //final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  bool navigateFromNotification = false;

  @override
  void initState() {
    super.initState();
    //_fcm.subscribeToTopic('user');
    //_fcm.subscribeToTopic('test');
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: true));
    }
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //message['notification']['tag'];
        /* Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => ProductsTab()));*/
        navigateFromNotification = true;
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(message['notification']['title'],
                textAlign: TextAlign.center),
            content: Text(message['notification']['body'],
                textAlign: TextAlign.justify),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        navigateFromNotification = true;
        if (message["data"] != null && message["data"]["type"] != null) {
          switch (message["data"]["type"]) {
            case "orderUpdate":
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => OrdersTab()));
              break;
            default:
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomeScreen()));
              break;
          }
        } else {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => HomeScreen()));
        }

        /* widget.isLogged
            ? Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen()))
            : Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => LoginScreen()));*/
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        navigateFromNotification = true;
        if (message["data"]["type"] != null) {
          switch (message["data"]["type"]) {
            case "orderUpdate":
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => OrdersTab()));
              break;
            default:
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomeScreen()));
              break;
          }
        } else {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => HomeScreen()));
        }
      },
      onBackgroundMessage: bgMessageHandler,
    );
    if (navigateFromNotification == false) {
      UserModel.of(context).isInitialLoggedIn().then((value) {
        Future.delayed(Duration(seconds: 2), () {
          if (navigateFromNotification == false) {
            value
                ? Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => HomeScreen()))
                : Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => LoginScreen()));
          }
        });
      }).catchError((error) {
        print(error.toString());
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => LoginScreen()));
      });
    }
  }

  static Future<dynamic> bgMessageHandler(Map<String, dynamic> message) {
    print('AppPushs myBackgroundMessageHandler : $message');
    return Future<void>.value();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      return Scaffold(
          backgroundColor: Theme.of(context).primaryColor,
          body: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Hero(
                    tag: "fundo",
                    child: Image.asset(
                      "images/Fundo2.png",
                      fit: BoxFit.cover,
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                    ),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    ),
                    margin: EdgeInsets.symmetric(
                        horizontal: 20.0,
                        vertical: MediaQuery.of(context).size.height * 0.28),
                    //color: Colors.white10,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            width: 140.0,
                            height: 140.0,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("images/logo.png"),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text: "Bem-Vindo(a) ao FAEN App\n",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.white,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "Aproveite : )",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white,
                                          fontSize: 16)),
                                ]),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Center(
                              child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          )),
                          SizedBox(
                            height: 5.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ));
    });
  }
}
