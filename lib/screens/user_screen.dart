import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/screens/login_signup_screens/login_screen.dart';
import 'package:faen_app_user/screens/student_card_screen.dart';
import 'package:faen_app_user/screens/user_edit_screen.dart';
import 'package:faen_app_user/tabs/orders_tab.dart';
import "package:flutter/material.dart";
//import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scoped_model/scoped_model.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    //_portraitModeOnly();
    //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      return Scaffold(
        backgroundColor: Colors.transparent,
        /* appBar: AppBar(
          centerTitle: true,
          title: Image.asset("images/formiga.png",
              fit: BoxFit.contain, width: 140, height: 70),
        ),*/
        body: ListView(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.45,
              width: MediaQuery.of(context).size.width,
              //color: Theme.of(context).primaryColor.withOpacity(0.70),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Minha Conta",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.3,
                    width: MediaQuery.of(context).size.height * 0.3,
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage: NetworkImage(model.userData["photo"]),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(model.userData["name"],
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 7.5,
                  ),
                  /*FlatButton(
                    onPressed: model.userData["typeUser"] != "Não Associado"
                        ? () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => StudentCardScreen()));
                          }
                        : () {
                            showAlertDialog(context);
                          },
                    child: Text(
                      "Minha Carterinha >",
                      style: TextStyle(color: Colors.white70),
                    ),
                  ),*/
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.3901,
              width: MediaQuery.of(context).size.width,
              color: Colors.black.withOpacity(0.70),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(FontAwesome5.edit, color: Colors.white),
                    title: Text("Editar Perfil",
                        style: TextStyle(color: Colors.white)),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              UserEditScreen(model.userData)));
                    },
                  ),
                  ListTile(
                    leading:
                        Icon(Icons.playlist_add_check, color: Colors.white),
                    title: Text("Meus Pedidos",
                        style: TextStyle(color: Colors.white)),
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => OrdersTab()));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.account_circle,
                        color: model.userData["typeUser"] != "Não Associado"
                            ? Colors.white
                            : Colors.grey),
                    title: Text("Minha Carteirinha",
                        style: TextStyle(
                            color: model.userData["typeUser"] != "Não Associado"
                                ? Colors.white
                                : Colors.grey)),
                    onTap: model.userData["typeUser"] != "Não Associado"
                        ? () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => StudentCardScreen()));
                          }
                        : () {
                            showAlertDialog(context);
                          },
                  ),
                  ListTile(
                    leading: Icon(FontAwesome.sign_out, color: Colors.red[900]),
                    title:
                        Text("Sair", style: TextStyle(color: Colors.red[900])),
                    onTap: () {
                      model.signOut();
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => LoginScreen()));
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Tudo bem!",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text("Você não tem acesso a Carteirinha!",
                textAlign: TextAlign.center),
            content: Text(
                "Somente usuários Associados FAEN podem acessar a Carteirinha.\n"
                "Realize o pagamento da sua Carteirinha, e/ou entre em contato com um membro Administrador FAEN.",
                textAlign: TextAlign.justify),
            actions: [botaoContinuar]);
      },
    );
  }

  /*void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }*/
}
