import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/screens/student_card_screen.dart';
import 'package:faen_app_user/tabs/partner_tab.dart';
import 'package:faen_app_user/tabs/products_tab.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'cart_screen.dart';
import 'feed_screen.dart';
import 'user_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController _pageController;

  int _page = 0;

  @override
  void initState() {
    super.initState();
    Firestore.instance.settings(persistenceEnabled: true);
    _pageController = PageController();
    _portraitModeOnly();
  }

  @override
  void dispose() {
    _pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //_portraitModeOnly();
    //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor:
          Colors.black.withOpacity(0.70), // navigation bar color
      systemNavigationBarDividerColor: Colors.black87,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor:
          Theme.of(context).primaryColor.withOpacity(0.5), // status bar color
    ));

    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      List<BottomNavigationBarItem> itemsNavigationBar = model.isLoggedIn()
          ? _getList1(model.userData["name"], model.userData["photo"])
          : _getList2();
      //model.saveDeviceToken();
      return WillPopScope(
        onWillPop: () {
          return SystemChannels.platform
                  .invokeMethod<void>('SystemNavigator.pop', true) ??
              false;
        },
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              child: Hero(
                tag: "fundo",
                child: Image.asset(
                  "images/Fundo2.png",
                  fit: BoxFit.cover,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                ),
              ),
            ),
            Positioned.fill(
              child: Scaffold(
                // drawer: CustomDrawer(_pageController),
                appBar: _page != 1
                    ? AppBar(
                        automaticallyImplyLeading: false,
                        centerTitle: true,
                        //title: Text("FAEN App"),
                        title: Padding(
                          padding: const EdgeInsets.all(35.0),
                          child: Hero(
                            tag: "appBar",
                            child: Image.asset("images/AppBarImage.png",
                                fit: BoxFit.contain),
                          ),
                        ), //, width: 140, height: 70),
                        //title: Text("FAEN"),
                      )
                    : null,
                backgroundColor: Colors.transparent,
                bottomNavigationBar: Theme(
                    data: Theme.of(context).copyWith(
                        //canvasColor: Color.fromARGB(255, 4, 64, 0),
                        canvasColor: Colors.black.withOpacity(0.7),
                        //primaryColor: Colors.white,
                        textTheme: Theme.of(context).textTheme.copyWith(
                            caption: TextStyle(color: Colors.white70))),
                    child: BottomNavigationBar(
                        //showUnselectedLabels: true,
                        // type: BottomNavigationBarType.shifting,
                        iconSize: 27.0,
                        selectedLabelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        unselectedItemColor: Colors.grey,
                        currentIndex: _page,
                        //currentIndex: _page < 5 ? _page : 0,
                        onTap: (p) {
                          _pageController.animateToPage(p,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.ease);
                        },
                        items: itemsNavigationBar)),
                body: SafeArea(
                  child: PageView(
                      controller: _pageController,
                      onPageChanged: (p) {
                        //if (p <= 4) {
                        setState(() {
                          _page = p;
                        });
                        /*} else {
                        _page = 0;
                      }*/
                      },
                      children: <Widget>[
                        FeedScreen(),
                        //ProductsTab(_pageController),
                        ProductsTab(),
                        PartnerTab(),
                        CartScreen(),
                        UserScreen(),
                        /* UserScreen(),
                        OrdersTab(),
                        StudentCardScreen(),*/
                      ]),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  List<BottomNavigationBarItem> _getList1(String name, String photo) {
    return [
      BottomNavigationBarItem(
          activeIcon: Icon(
            FontAwesome5Solid.home,
          ),
          icon: Icon(
            FontAwesome.home,
          ),
          title: Text("Home")),
      BottomNavigationBarItem(
          //activeIcon: Icon(FontAwesome5Solid.shopping_bag),
          icon: Icon(
            FontAwesome5Solid.shopping_bag,
          ),
          title: Text("Loja")),
      BottomNavigationBarItem(
          activeIcon: Icon(
            FontAwesome5Solid.handshake,
          ),
          icon: Icon(
            FontAwesome.handshake_o,
          ),
          title: Text("Parceiros")),
      BottomNavigationBarItem(
          activeIcon: Icon(
            Icons.shopping_cart,
          ),
          icon: Icon(
            Icons.shopping_cart,
          ),
          title: Text("Carrinho")),
      /*BottomNavigationBarItem(
                    icon: Icon(Icons.playlist_add_check),
                    title: Text("Pedidos")),*/
      BottomNavigationBarItem(
        title: Text(name ?? ""),
        //ImageIcon(),
        icon: GestureDetector(
          onDoubleTap:
              UserModel.of(context).userData["typeUser"] != "Não Associado"
                  ? () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => StudentCardScreen()));
                    }
                  : null,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            backgroundImage: photo != null ? NetworkImage(photo) : null,
            /*child: photo != null
                  ? null
                  : Text(
                      name.substring(0, 1),
                    )*/
          ),
        ),
      ),
    ];
  }

  List<BottomNavigationBarItem> _getList2() {
    return [
      BottomNavigationBarItem(
          activeIcon: Icon(
            FontAwesome5Solid.home,
          ),
          icon: Icon(
            FontAwesome.home,
          ),
          title: Text("Home")),
      BottomNavigationBarItem(
          //activeIcon: Icon(FontAwesome5Solid.shopping_bag),
          icon: Icon(
            FontAwesome5Solid.shopping_bag,
          ),
          title: Text("Loja")),
      BottomNavigationBarItem(
          activeIcon: Icon(
            FontAwesome5Solid.handshake,
          ),
          icon: Icon(
            FontAwesome.handshake_o,
          ),
          title: Text("Parceiros")),
      BottomNavigationBarItem(
          activeIcon: Icon(
            Icons.shopping_cart,
          ),
          icon: Icon(
            Icons.shopping_cart,
          ),
          title: Text("Carrinho")),
      BottomNavigationBarItem(
          icon: Icon(Icons.playlist_add_check), title: Text("Pedidos")),
    ];
  }

  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
