import "package:flutter/material.dart";

import 'home_screen.dart';

class OrderScreen extends StatelessWidget {
  final String orderId;

  OrderScreen(this.orderId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pedido Realizado"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green[600],
        child: Icon(Icons.arrow_forward_ios),
        onPressed: () {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        },
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.check_circle_outline,
              color: Colors.green[600],
              size: 88.0,
            ),
            Text(
              "Pedido realizado com sucesso !",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
            SelectableText("Código do pedido: $orderId",
                style: TextStyle(fontSize: 16.0))
          ],
        ),
      ),
    );
  }
}
