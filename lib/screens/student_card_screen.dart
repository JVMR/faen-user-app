import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';

import '../SizeConfig.dart';

class StudentCardScreen extends StatefulWidget {
  @override
  _StudentCardScreenState createState() => _StudentCardScreenState();
}

class _StudentCardScreenState extends State<StudentCardScreen> {
  //String expiresDate = "";
  @override
  void initState() {
    super.initState();
    /*var doc = await Firestore.instance
        .collection("association")
        .document(DateTime.now().year.toString())
        .get();
    expiresDate = doc.data["expiresDate"];*/
    _landscapeModeOnly();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  void dispose() {
    super.dispose();
    _portraitModeOnly();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {
    //_landscapeModeOnly();
    SizeConfig().init(context);
    SystemChrome.setEnabledSystemUIOverlays([]);
    /*Widget _buildCardBack() => Container(
          decoration: BoxDecoration(
            // border: Border.all(color: Colors.white70),
            borderRadius: BorderRadius.circular(15.0),
            gradient: LinearGradient(
              colors: [
                Colors.black,
                Theme.of(context).primaryColor,
                //Colors.green,
              ],
              stops: [0.4, 1],
              begin: Alignment.topLeft,
              end: Alignment.bottomCenter,
            ),
          ),
        );*/
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "images/Fundo2.png",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Positioned.fill(
          child: Container(color: Colors.black87),
        ),
        Positioned(
          //top: 5.0,
          //left: 5.0,
          child: FlatButton.icon(
              // color: Colors.white,
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                  Theme.of(context).platform == TargetPlatform.iOS
                      ? Icons.arrow_back_ios
                      : Icons.arrow_back,
                  color: Colors.white),
              label: Text("Voltar", style: TextStyle(color: Colors.white70))),
        ),
        Positioned.fill(
          child: ScopedModelDescendant<UserModel>(
            builder: (context, child, model) {
              return Card(
                elevation: 9.0,
                margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.height * 0.15,
                  vertical: MediaQuery.of(context).size.width * 0.05,
                ),
                shape: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white10),
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Stack(
                  // fit: StackFit.expand,
                  children: <Widget>[
                    Positioned.fill(
                      bottom: 0.0,
                      child: Image.asset(
                        "images/FundoCarteirinha.png",
                        fit: BoxFit.fill,
                        //height: MediaQuery.of(context).size.height,
                        //width: MediaQuery.of(context).size.width,
                      ),
                      // child: _buildCardBack(),
                    ),
                    Positioned.fill(
                      //top: 5,
                      //right: SizeConfig.safeBlockVertical * 10,
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Membro Associado",
                          style: TextStyle(
                            fontFamily: "MovingSkate",
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockHorizontal * 10,
                            // fontSize: 20.0,
                            //fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 15,
                      right: 5,
                      child: Container(
                        width: SizeConfig.safeBlockHorizontal * 15,
                        height: SizeConfig.safeBlockHorizontal * 15,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("images/logo.png"),
                              fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 13,
                      top: MediaQuery.of(context).size.width * 0.085,
                      child: Container(
                        height: MediaQuery.of(context).size.width * 0.2,
                        width: MediaQuery.of(context).size.width * 0.2,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.white70),
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                              image:
                                  NetworkImage(model.userData["photo"] ?? ""),
                              fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    Positioned(
                      left: MediaQuery.of(context).size.width * 0.23,
                      top: MediaQuery.of(context).size.width * 0.09,
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            // SizedBox(height: 10.0),
                            /* Text(
                                "Membro " + model.userData["typeUser"] + " FAEN",
                                style: TextStyle(
                                    fontFamily: "Moving Skate",
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold),
                              ),*/
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                lineText("Nome:\n", model.userData["name"]),
                                lineText("CPF:\n", model.userData["cpf"]),
                                lineText("Curso:\n", model.userData["course"]),
                              ],
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                lineText(
                                    "Data de Nasc.:\n", model.userData["date"]),
                                lineText("Semestre:\n",
                                    model.userData["semester"].toString()),
                                FutureBuilder(
                                  future: Firestore.instance
                                      .collection("association")
                                      .document(DateTime.now().year.toString())
                                      .get(),
                                  builder: (context, snapshot) {
                                    if (!snapshot.hasData)
                                      return Container();
                                    else
                                      return lineText(
                                          "Validade:\n",
                                          snapshot.data["expiresDate"]
                                                  .toString()
                                                  .substring(0, 10) ??
                                              "");
                                  },
                                ),
                              ],
                            ),
                            //lineText("Validade: ", "01/03/2021"),
                            /*   SizedBox(
                                height: MediaQuery.of(context).size.width * 0.10,
                              ),*/
                          ],
                        ),
                      ),
                    ),
                    Positioned.fill(
                      // left: MediaQuery.of(context).size.width * 0.23,
                      bottom: 10,
                      // right: 5,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: _rowWithLogos(),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _rowWithLogos() {
    double height = SizeConfig.safeBlockHorizontal * 8;
    double width = SizeConfig.safeBlockHorizontal * 8;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(width: width * 0.2),
        Image.asset(
          "images/LogoE-sports.png",
          fit: BoxFit.contain,
          width: width,
          height: height,
          //height: MediaQuery.of(context).size.height,
          //width: MediaQuery.of(context).size.width,
        ),
        //SizedBox(width: width * 0.5),
        Image.asset(
          "images/LogoTorcidaOrganizada.png",
          fit: BoxFit.contain,
          width: width,
          height: height,
          //height: MediaQuery.of(context).size.height,
          //width: MediaQuery.of(context).size.width,
        ),
        //SizedBox(width: width * 0.5),
        Image.asset(
          "images/LogoCheers.png",
          fit: BoxFit.contain,
          width: width,
          height: height,
          //height: MediaQuery.of(context).size.height,
          //width: MediaQuery.of(context).size.width,
        ),
        //SizedBox(width: width * 0.5),
        Image.asset(
          "images/LogoBateria.png",
          fit: BoxFit.contain,
          width: width,
          height: height,
          //width: MediaQuery.of(context).size.width,
        ),
        SizedBox(width: width * 0.2),
      ],
    );
  }

  Widget lineText(String title, String userData) {
    return Padding(
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: RichText(
        text: TextSpan(
            text: title,
            style: TextStyle(
              fontFamily: "BebasNeue",
              fontSize: SizeConfig.safeBlockHorizontal * 2,
            ),
            children: <TextSpan>[
              TextSpan(
                text: userData,
                style: TextStyle(
                  color: Colors.green[800],
                  fontFamily: "BebasNeue",
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.safeBlockHorizontal * 2.5,
                ),
              )
            ]),
      ),
    );
  }

  void _landscapeModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
