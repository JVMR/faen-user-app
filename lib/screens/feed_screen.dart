import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/widgets/cards/feed_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class FeedScreen extends StatefulWidget {
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  @override
  Widget build(BuildContext context) {
    if (UserModel.of(context).userData["typeUser"] != "Não Associado") {
      return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance
            .collection("feed")
            .orderBy("createdAt", descending: true)
            .getDocuments(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            );
          else if (snapshot.data.documents.length == 0)
            return Center(
              child: Text(
                "Não há nenhum post no Feed no momento !",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            );
          else
            return AnimationLimiter(
              child: ListView.builder(
                padding: EdgeInsets.all(4.0),
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) {
                  /*FeedData data =
                      FeedData.fromDocument(snapshot.data.documents[index]);*/
                  return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 375),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                            child: FeedCard(
                                snapshot.data.documents[index], update)),
                      ));
                },
              ),
            );
        },
      );
    } else {
      return Center(
        child: Text(
          "Somente membros Associados podem visualizar\no Feed de Notícias : (",
          textAlign: TextAlign.center,
          softWrap: true,
          style: TextStyle(color: Colors.red[900], fontSize: 16),
        ),
      );
    }
  }

  VoidCallback update() {
    setState(() {});
    return null;
  }
}
