import 'package:carousel_pro/carousel_pro.dart';
import 'package:faen_app_user/datas/cart_product.dart';
import 'package:faen_app_user/datas/products_data.dart';
import 'package:faen_app_user/models/cart_model.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:flutter/material.dart';
import 'cart_screen.dart';
import 'login_signup_screens/login_screen.dart';

class ProductScreen extends StatefulWidget {
  final ProductData product;

  static String tamanho;

  ProductScreen(this.product);

  @override
  _ProductScreenState createState() => _ProductScreenState(product);
}

class _ProductScreenState extends State<ProductScreen> {
  final ProductData product;
  String tamanho;
  _ProductScreenState(this.product);

  @override
  Widget build(BuildContext context) {
    final Color primaryColor = Theme.of(context).primaryColor;

    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "images/Fundo2.png",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Positioned.fill(
          child: Scaffold(
            appBar: AppBar(
              title: Text(product.title),
              centerTitle: true,
              backgroundColor: Colors.black.withOpacity(0.80),
            ),
            backgroundColor: Colors.black.withOpacity(0.80),
            body: ListView(
              children: <Widget>[
                Hero(
                  tag: product.title,
                  child: AspectRatio(
                    aspectRatio: 0.90,
                    child: Carousel(
                      images: product.images.map((url) {
                        /* return GestureDetector(
                            onDoubleTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => PhotoViewScreen(url)));
                            },
                            child: Image.network(url));*/
                        return NetworkImage(url);
                      }).toList(),
                      dotSize: 4.0,
                      dotSpacing: 15.0,
                      /*dotBgColor: Colors.transparent,
                      dotColor: primaryColor,*/
                      dotBgColor: Colors.transparent,
                      dotColor: Colors.white70,
                      autoplay: false,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        product.title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500,
                        ),
                        maxLines: 3,
                      ),
                      Text(
                        "R\$ ${product.priceMember.toStringAsFixed(2)} para Sócios",
                        style: TextStyle(
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.greenAccent[700],
                        ),
                      ),
                      Text(
                        "R\$ ${product.price.toStringAsFixed(2)} para Não Sócios",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.normal,
                          //color: primaryColor,
                        ),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      Visibility(
                        visible: product.hasSize,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              "Tamanho",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(
                              height: 34.0,
                              child: GridView(
                                padding: EdgeInsets.symmetric(vertical: 4.0),
                                scrollDirection: Axis.horizontal,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 1,
                                  crossAxisSpacing: 8.0,
                                  childAspectRatio: 0.5,
                                ),
                                children: product.sizes.map((t) {
                                  return GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        tamanho = t;
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: t == tamanho
                                              ? Colors.greenAccent[700]
                                              : Colors.grey[500],
                                          width: 3.0,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(4.0)),
                                      ),
                                      width: 50.0,
                                      alignment: Alignment.center,
                                      child: Text(t ?? "",
                                          style:
                                              TextStyle(color: Colors.white)),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      SizedBox(
                        height: 44.0,
                        child: RaisedButton(
                          shape: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(122, 4, 64, 0)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12.5)),
                          ),
                          onPressed: !product.hasSize || tamanho != null
                              ? () {
                                  if (UserModel.of(context).isLoggedIn()) {
                                    CartProduct cartProduct = CartProduct();
                                    cartProduct.size = tamanho;
                                    cartProduct.quantity = 1;
                                    cartProduct.pid = product.id;
                                    cartProduct.category = product.category;
                                    cartProduct.productData = product;

                                    CartModel.of(context)
                                        .addCartItem(cartProduct);
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                CartScreen()));
                                  } else {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                LoginScreen()));
                                  }
                                }
                              : null,
                          child: Text(
                            UserModel.of(context).isLoggedIn()
                                ? "Adicionar ao Carrinho"
                                : "Entre para comprar",
                            style: TextStyle(
                              // color: Colors.black,
                              fontSize: 18.0,
                            ),
                          ),
                          color: Colors.white,
                          textColor: primaryColor,
                          disabledTextColor: Colors.white60,
                        ),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      Text(
                        "Descrição",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        product.description,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
