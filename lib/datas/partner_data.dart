import 'package:cloud_firestore/cloud_firestore.dart';

class PartnerData {
  String id;

  String title;
  String subTitle;
  String description;

  String image;
  PartnerData.fromDocument(DocumentSnapshot snapshot) {
    id = snapshot.documentID;
    title = snapshot.data["title"];
    subTitle = snapshot.data["subTitle"];
    description = snapshot.data["description"];
  image = snapshot.data["image"];
  }
}
