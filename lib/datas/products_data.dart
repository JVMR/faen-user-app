import 'package:cloud_firestore/cloud_firestore.dart';

class ProductData {
  String id;
  String category;

  String title;
  String description;
  String typeUser;
  String status;

  double price;
  double priceMember;

  bool hasSize;

  List images;
  List sizes;
  ProductData.fromDocument(DocumentSnapshot snapshot, String typeUser) {
    this.typeUser = typeUser;
    id = snapshot.documentID;
    title = snapshot.data["title"];
    description = snapshot.data["description"];
    price = snapshot.data["price"] + 0.0;
    priceMember = snapshot.data["priceMember"] + 0.0;
    images = snapshot.data["images"];
    hasSize = snapshot.data["hasSize"];
    sizes = snapshot.data["sizes"];
    status =  snapshot.data["status"] ?? "";
  }

  Map<String, dynamic> toResumeMap() {
    return {
      "title": title,
      "description": description,
      "price": price,
    };
  }
}
