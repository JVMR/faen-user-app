import 'package:faen_app_user/screens/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'models/cart_model.dart';
import 'models/user_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: ScopedModelDescendant<UserModel>(builder: (context, child, model) {
        // model.saveDeviceToken();
        // final home = !model.isLoggedIn() ? LoginScreen() : HomeScreen();
        //Future.delayed(Duration(seconds: 3), () {});
        return ScopedModel<CartModel>(
          model: CartModel(model),
          child: MaterialApp(
              title: 'FAEN User',
              theme: ThemeData(
                  cursorColor: Color.fromARGB(255, 4, 64, 0),
                  focusColor: Colors.green[900],
                  errorColor: Colors.red[900],
                  fontFamily: "BebasNeue",
                  textTheme: TextTheme(
                    bodyText1: TextStyle(color: Colors.white),
                  ),
                  cardColor: Colors.black.withOpacity(0.70),
                  textSelectionColor: Colors.green[700],
                  textSelectionHandleColor: Colors.green[900],
                  // primarySwatch: Colors.white,
                  //   primaryColor: Color.fromARGB(255, 4, 64, 0)),
                  primaryColor: Colors.black),
              debugShowCheckedModeBanner: false,
              //home: HomeScreen(),
              home: WelcomeScreen()),
        );
      }),
    );
  }
}
