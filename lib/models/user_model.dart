import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class UserModel extends Model {
  //usuário atual

  FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  FirebaseUser firebaseUser;
  Map<String, dynamic> userData = Map();
  bool assIsOpen;

  bool isLoading = false;
  static UserModel of(BuildContext context) =>
      ScopedModel.of<UserModel>(context);

  @override
  void addListener(VoidCallback listener) async {
    super.addListener(listener);
    await associationIsOpen();
    await _loadCurrentUser();
  }

  dispose() {
    iosSubscription.cancel();
  }

  void signUp(
      {@required Map<String, dynamic> userData,
      @required String pass,
      @required VoidCallback onSuccess,
      @required VoidCallback onFail}) async {
    isLoading = true;
    notifyListeners();
    _auth
        .createUserWithEmailAndPassword(
            email: userData["email"], password: pass)
        .then((user) async {
      firebaseUser = user.user;
      await _saveUserData(userData);
      onSuccess();
      isLoading = false;
      notifyListeners();
    }).catchError((erro) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

  void signIn(
      {@required String email,
      @required String pass,
      @required VoidCallback onSuccess,
      @required VoidCallback onFail}) async {
    isLoading = true;
    notifyListeners();
    _auth
        .signInWithEmailAndPassword(email: email, password: pass)
        .then((user) async {
      firebaseUser = user.user;
      await _loadCurrentUser();
      onSuccess();
      isLoading = false;
      notifyListeners();
    }).catchError((e) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

  void recoverPass(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  Future<bool> updateEmail(String newEmail) async {
    try {
      await firebaseUser.updateEmail(newEmail);
      /*  _db.collection("users").document(firebaseUser.uid).updateData({
        "email": newEmail,
      });
      await _loadCurrentUser();*/
      return true;
    } catch (error) {
      print(error.toString());
      return false;
    }
  }

  void signOut() async {
    await _auth.signOut();

    userData = Map();
    firebaseUser = null;
    notifyListeners();
  }

  Future<bool> isInitialLoggedIn() async {
    return firebaseUser != null || await _auth.currentUser() != null;
  }

  bool isLoggedIn() {
    return firebaseUser != null;
  }

  Future<bool> associationIsOpen() async {
    //Retorna true se as associações estiverem abertas
    //print("Chamou o método");
    var doc = await Firestore.instance
        .collection("association")
        .document(DateTime.now().year.toString())
        .get();
    assIsOpen = doc.data["state"] == "Aberto";
    notifyListeners();
    return doc.data["state"] == "Aberto";
  }

  Future<Null> _saveUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    await _uploadImage();
    await Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .setData(userData);
  }

  Future<bool> updateUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    isLoading = true;
    try {
      await _uploadImage();
      await Firestore.instance
          .collection("users")
          .document(firebaseUser.uid)
          .updateData(userData);
      _loadCurrentUser();
      isLoading = false;
      return true;
    } catch (e) {
      print(e.toString());
      isLoading = false;
      return false;
    }
  }

  Future _uploadImage() async {
    if (userData["photo"] is File) {
      StorageUploadTask uploadTask = FirebaseStorage.instance
          .ref()
          .child("Users Photos")
          .child(userData["name"])
          .putFile(userData["photo"]);

      StorageTaskSnapshot s = await uploadTask.onComplete;
      String downloadUrl = await s.ref.getDownloadURL();

      userData["photo"] = downloadUrl;
    }
  }

  Future<Null> _loadCurrentUser() async {
    if (firebaseUser == null) firebaseUser = await _auth.currentUser();
    if (firebaseUser != null) {
      if (userData["name"] == null) {
        DocumentSnapshot docUser = await Firestore.instance
            .collection("users")
            .document(firebaseUser.uid)
            .get();
        userData = docUser.data;
        notifyListeners();
        /*userData["tokens"] = await Firestore.instance
            .collection("users")
            .document(firebaseUser.uid)
            .collection("tokens")
            .document(await _fcm.getToken())
            .get();*/
        var querySnapshot = await Firestore.instance
            .collection('users')
            .document(firebaseUser.uid)
            .collection("tokens")
            .orderBy("createdAt")
            .getDocuments();
        if (querySnapshot.documents.length > 0) {
          userData["tokens"] =
              querySnapshot.documents.map((snap) => snap.documentID).last;
        }
        print("Último token salvo no banco: ${userData["tokens"]}");
        if (Platform.isIOS) {
          iosSubscription = _fcm.onIosSettingsRegistered.listen((data) async {
            // save the token  OR subscribe to a topic here
            await saveDeviceToken();
            await subscribeToTopics();
          });
          _fcm.requestNotificationPermissions(IosNotificationSettings(
              sound: true, badge: true, alert: true, provisional: true));
        } else {
          await saveDeviceToken();
          await subscribeToTopics();
        }
      }
    }
    notifyListeners();
  }

  Future<void> subscribeToTopics() async {
    _fcm.subscribeToTopic('user'); //Todos os usuários
    // _fcm.subscribeToTopic('devTest');
    if (userData["typeUser"] == "Administrador") {
      _fcm.unsubscribeFromTopic('non_members');
      _fcm.subscribeToTopic('members');
      _fcm.subscribeToTopic('adms');
      _fcm.subscribeToTopic('test');
    } else if (userData["typeUser"] != "Não Associado") {
      _fcm.unsubscribeFromTopic('non_members');
      _fcm.subscribeToTopic('members');
    } else {
      _fcm.subscribeToTopic('non_members');
    }
  }

  Future<void> saveDeviceToken() async {
    String fcmToken = await _fcm.getToken();
    print("As chaves são iguais ? ${fcmToken == userData["tokens"]}");
    if (userData["tokens"] == null ||
        fcmToken != userData["tokens"] && firebaseUser != null) {
      //firebaseUser = await _auth.currentUser();

      // Get the token for this device

      print("Token do FCM: $fcmToken");
      print("Último token salvo: ${userData["tokens"]}");
      /*print(
          "As chaves são iguais ${fcmToken == userData["tokens"].documentID}");*/

      // Save it to Firestore
      if (fcmToken != null) {
        print("Entrou para salvar o token");
        var tokens = _db
            .collection('users')
            .document(firebaseUser.uid)
            .collection('tokens')
            .document(fcmToken);

        await tokens.setData({
          'token': fcmToken,
          'createdAt': FieldValue.serverTimestamp(), // optional
          'platform': Platform.operatingSystem // optional
        });
      }
    }
  }
}
