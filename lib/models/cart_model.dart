import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/datas/cart_product.dart';
import 'package:faen_app_user/models/notification_sender.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'user_model.dart';

class CartModel extends Model {
  UserModel user;
  List<CartProduct> products = [];

  bool isLoading = false;
  Firestore _firestore = Firestore.instance;
  NotificationSender _notificationSender = NotificationSender();

  String couponCode;
  int discountPercentage = 0;

  CartModel(this.user) {
    if (user.isLoggedIn()) {
      _loadCartItems();
    }
  }

  static CartModel of(BuildContext context) =>
      ScopedModel.of<CartModel>(context);

  void addCartItem(CartProduct cartProduct) {
    products.add(cartProduct);
    _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("cart")
        .add(cartProduct.toMap())
        .then((doc) {
      cartProduct.cid = doc.documentID;
    });
    notifyListeners();
  }

  void removeCartItem(CartProduct cartProduct) {
    _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("cart")
        .document(cartProduct.cid)
        .delete();
    products.remove(cartProduct);
    notifyListeners();
  }

  void decProduct(CartProduct cartProduct) {
    cartProduct.quantity--;
    _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("cart")
        .document(cartProduct.cid)
        .updateData(cartProduct.toMap());
    notifyListeners();
  }

  void incProduct(CartProduct cartProduct) {
    cartProduct.quantity++;
    _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("cart")
        .document(cartProduct.cid)
        .updateData(cartProduct.toMap());
    notifyListeners();
  }

  void setCoupon(String couponCode, int discountPercentage) {
    this.couponCode = couponCode;
    this.discountPercentage = discountPercentage;
  }

  void updatePrices() {
    notifyListeners();
  }

  double getProductsPrice() {
    double price = 0.0;
    for (CartProduct c in products) {
      if (c.productData != null) {
        double productPrice = c.productData.typeUser == "Não Associado"
            ? c.productData.price
            : c.productData.priceMember;
        price += c.quantity * productPrice;
      }
    }
    return price;
  }

  double getDiscount() {
    return getProductsPrice() * discountPercentage / 100;
  }

  Future<String> finishOrder(String typePay) async {
    if (products.length == 0) return null;
    isLoading = true;
    notifyListeners();
    double productsPrice = getProductsPrice();
    double discount = getDiscount();
    var now = DateTime.now();
    String orderDate = DateFormat("dd/MM/yyyy").format(now);

    DocumentReference refOrder = await _firestore.collection("orders").add({
      "clientId": user.firebaseUser.uid,
      "products": products.map((cartProduct) => cartProduct.toMap()).toList(),
      "productsPrice": productsPrice,
      "discount": discount,
      "totalPrice": productsPrice - discount,
      "status": 1,
      "typePay": typePay,
      "orderDate": orderDate
    });
    await _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("orders")
        .document(refOrder.documentID)
        .setData({"orderId": refOrder.documentID});
    QuerySnapshot query = await _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("cart")
        .getDocuments();

    // await updateOrderCount();
    await sendNotification();
    await updateProductsSolds();

    for (DocumentSnapshot doc in query.documents) {
      //Limpa o carrinho salvo no doc do usuário
      doc.reference.delete();
    }

    products.clear();
    discountPercentage = 0;
    couponCode = null;
    isLoading = false;
    notifyListeners();
    return refOrder.documentID;
  }

  Future<void> sendNotification() async {
    bool sucess = await _notificationSender.sendNoticationToTopic(
        title: "Novo Pedido recebido !",
        body:
            "Pedido feito por ${user.userData["name"]}.\nVeja mais informações no seu FAEN Admin App.",
        topic: "test");
    print("Enviou a notificação: $sucess");
  }

  void _loadCartItems() async {
    QuerySnapshot query = await _firestore
        .collection("users")
        .document(user.firebaseUser.uid)
        .collection("cart")
        .getDocuments();

    products =
        query.documents.map((doc) => CartProduct.fromDocument(doc)).toList();
    notifyListeners();
  }

  Future<void> updateOrderCount() async {
    String currentDate = "${DateTime.now().month}.${DateTime.now().year}";

    try {
      var docMonth =
          await _firestore.collection("revenue").document(currentDate).get();
      if (docMonth == null || !docMonth.exists) {
        print("entro no if");
        await createDocumenteRevenue(currentDate);
      } else {
        updateProductsSolds();
        /* int totalOrder = docMonth.data["totalOrder"];
        totalOrder++;
        await _firestore
            .collection("revenue")
            .document(currentDate)
            .updateData({"totalOrder": totalOrder});*/
      }
    } catch (e) {
      print("===" + e.toString() + "===");
    }
  }

  Future<void> updateProductsSolds() async {
    Map<String, dynamic> productsSoldToUpdate = {};
    DocumentReference revenueRef = _firestore
        .collection("revenue")
        .document("${DateTime.now().month}.${DateTime.now().year}");
    await revenueRef.get().then((doc) {
      productsSoldToUpdate = doc.data;
    });
    print(productsSoldToUpdate);
    for (CartProduct c in products) {
      if (c.productData != null) {
        productsSoldToUpdate["productsSold"][c.productData.title] += c.quantity;
      }
    }
    productsSoldToUpdate["totalOrder"]++;
    print(productsSoldToUpdate);
    await revenueRef.updateData(productsSoldToUpdate);
  }

  Future<void> createDocumenteRevenue(String currentDate) async {
    List<String> titlesProducts = [];
    Map<String, dynamic> productsSold = {};
    print("Aloha");
    var categories = await _firestore.collection('products').getDocuments();
    var products = categories.documents.map((category) async {
      return await _firestore
          .collection('products')
          .document(category.documentID)
          .collection("items")
          .getDocuments();
    });

    for (var productsQuery in products) {
      print("Entrou no For Each");
      await productsQuery.then((productsSnap) {
        productsSnap.documents.forEach((product) {
          print(product.data["title"]);
          titlesProducts.add(product.data["title"]);
          productsSold[product.data["title"]] = 0;
        });
      });
    }

    print(titlesProducts);
    print(productsSold);

    Map<String, dynamic> monthInitialData = {
      'totalOrder':
          1, //1 definido pois o documento será criado quando haver o primeiro pedido
      'totalOrderCash': 0.0,
      'titlesProducts': titlesProducts,
      'productsSold': productsSold,
    };
    await _firestore
        .collection('revenue')
        .document(currentDate)
        .setData(monthInitialData);
  }
}
