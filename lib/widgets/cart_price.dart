import 'package:faen_app_user/models/cart_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class CartPrice extends StatelessWidget {
  final VoidCallback buy;

  CartPrice(this.buy);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: ScopedModelDescendant<CartModel>(
          builder: (context, child, model) {
            double price = model.getProductsPrice();
            double discount = model.getDiscount();
            //double ship = model.getShipPrice();

            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  "Resumo do Pedido",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 12.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Subtotal",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "R\$ ${price.toStringAsFixed(2)}",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Desconto",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "R\$ ${discount.toStringAsFixed(2)}",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
                Divider(),
                /*Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Entrega"),
                    Text("R\$ ${ship.toStringAsFixed(2)}")
                  ],
                ),
                Divider(),*/
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Total",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      "R\$ ${(price - discount).toStringAsFixed(2)}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.greenAccent[700],
                          fontSize: 16.0),
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    RaisedButton(
                      shape: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(122, 4, 64, 0)),
                        borderRadius: BorderRadius.all(Radius.circular(12.5)),
                      ),
                      child: Text("Finalizar Pedido"),
                      textColor: Theme.of(context).primaryColor,
                      color: Colors.white,
                      onPressed: buy,
                    ),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
