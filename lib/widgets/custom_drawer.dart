import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/screens/login_signup_screens/login_screen.dart';
import 'package:faen_app_user/widgets/tiles/drawer_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scoped_model/scoped_model.dart';

class CustomDrawer extends StatelessWidget {
  final pageController;

  CustomDrawer(this.pageController);
  @override
  Widget build(BuildContext context) {
    bool isAdm = true;
    return Drawer(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          //_buildDrawerBack(),
          /*Image.asset(
            "images/fundo_drawer.png",
            fit: BoxFit.cover,
          ),*/
          Positioned(
            bottom: 15,
            right: 5,
            child: Container(
              width: 70.0,
              height: 70.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/logo.png"), fit: BoxFit.cover),
              ),
            ),
          ),
          ListView(
            padding: EdgeInsets.only(left: 25.0, top: 16.0),
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 8.0),
                padding: EdgeInsets.fromLTRB(0.0, 16.0, 16.0, 8.0),
                height: 170.0,
                child: ScopedModelDescendant<UserModel>(
                    builder: (context, child, model) {
                  isAdm = model.userData["typeUser"] != "Não Associado"
                      ? true
                      : false;
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            height: 80,
                            width: 80,
                            child: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              backgroundImage: model.userData["photo"] != null
                                  ? NetworkImage(model.userData["photo"])
                                  : null,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "${!model.isLoggedIn() ? "" : model.userData["name"]}",
                                style: TextStyle(
                                    color: Colors.white70,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                                softWrap: false,
                                //maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                model.isLoggedIn()
                                    ? "Membro: " + model.userData["typeUser"]
                                    : "",
                                style: TextStyle(
                                  color: Colors.white70,
                                  fontSize: 14,
                                  fontStyle: FontStyle.italic,
                                ),
                                softWrap: true,
                                maxLines: 2,
                                overflow: TextOverflow.fade,
                              ),
                              GestureDetector(
                                child: Text(
                                  !model.isLoggedIn()
                                      ? "Entre ou cadastre-se"
                                      : "Sair",
                                  style: TextStyle(
                                    color: Colors.white54,
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onTap: () {
                                  if (!model.isLoggedIn()) {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                LoginScreen()));
                                  } else {
                                    model.signOut();
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                LoginScreen()));
                                  }
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  );
                }),
              ),
              Divider(),
              /*DrawerTile(FontAwesome5Solid.home, "Home", pageController, 0),
              DrawerTile(
                  FontAwesome5Solid.shopping_bag, "Loja", pageController, 1),
              DrawerTile(
                  FontAwesome5Solid.handshake, "Parceiros", pageController, 2),
              DrawerTile(Icons.shopping_cart, "Carrinho", pageController, 3),*/
              DrawerTile(FontAwesome.gear, "Ajustes", pageController, 4),
              DrawerTile(
                  Icons.playlist_add_check, "Pedidos", pageController, 5),
              Visibility(
                visible: isAdm,
                child: DrawerTile(Icons.account_circle, "Minha Carteirinha",
                    pageController, 6),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
