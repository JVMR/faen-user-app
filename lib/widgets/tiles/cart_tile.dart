import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/datas/cart_product.dart';
import 'package:faen_app_user/datas/products_data.dart';
import 'package:faen_app_user/models/cart_model.dart';
import 'package:faen_app_user/models/user_model.dart';
import 'package:flutter/material.dart';

class CartTile extends StatelessWidget {
  final CartProduct cartProduct;

  CartTile(this.cartProduct);

  @override
  Widget build(BuildContext context) {
    Widget _buildContent() {
      CartModel.of(context).updatePrices();
      double price = cartProduct.productData.typeUser == "Não Associado"
          ? cartProduct.productData.price
          : cartProduct.productData.priceMember;
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Hero(
              tag: cartProduct.productData.title,
              child: Container(
                //padding: EdgeInsets.all(8.0),
                width: 120.0,
                height: 120.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Color.fromARGB(122, 4, 64, 0)),
                  borderRadius: BorderRadius.circular(15.0),
                  image: DecorationImage(
                      image: NetworkImage(cartProduct.productData.images[0]),
                      fit: BoxFit.cover),
                ),
                /*child: Image.network(
                  cartProduct.productData.images[0],
                  fit: BoxFit.cover,
                ),*/
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    cartProduct.productData.title,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.w500),
                  ),
                  Visibility(
                    visible: cartProduct.size != null,
                    child: Text(
                      "Tamanho: ${cartProduct.size}",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w300),
                    ),
                  ),
                  Text(
                    "R\$ ${price.toStringAsFixed(2)}",
                    style: TextStyle(
                        color: Colors.greenAccent[700],
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.remove,
                          color: Colors.white,
                        ),
                        onPressed: cartProduct.quantity > 1
                            ? () {
                                CartModel.of(context).decProduct(cartProduct);
                              }
                            : null,
                        color: Theme.of(context).primaryColor,
                      ),
                      Text(cartProduct.quantity.toString(),
                          style: TextStyle(
                            color: Colors.white,
                          )),
                      IconButton(
                        icon: Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          CartModel.of(context).incProduct(cartProduct);
                        },
                      ),
                      FlatButton(
                        shape: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(122, 4, 64, 0)),
                          borderRadius: BorderRadius.all(Radius.circular(12.5)),
                        ),
                        child: Text("Remover"),
                        textColor: Colors.black54,
                        onPressed: () {
                          CartModel.of(context).removeCartItem(cartProduct);
                        },
                        color: Colors.white,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    }

    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: cartProduct.productData == null
          ? FutureBuilder<DocumentSnapshot>(
              future: Firestore.instance
                  .collection("products")
                  .document(cartProduct.category)
                  .collection("items")
                  .document(cartProduct.pid)
                  .get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  cartProduct.productData = ProductData.fromDocument(
                      snapshot.data,
                      UserModel.of(context).userData["typeUser"]);
                  return _buildContent();
                } else {
                  return Container(
                    height: 70.0,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ),
                    alignment: Alignment.center,
                  );
                }
              },
            )
          : _buildContent(),
    );
  }
}
