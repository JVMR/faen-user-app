import 'dart:ui';

import 'package:faen_app_user/datas/products_data.dart';
import 'package:faen_app_user/screens/product_screen.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class ProductTile extends StatelessWidget {
  final String type;
  final ProductData product;

  ProductTile(this.type, this.product);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.28,
      child: Card(
        elevation: 8.0,
        color: Colors.black.withOpacity(0.80),
        margin: EdgeInsets.all(5.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
        ),
        child: InkWell(
          onTap: product.status == "Disponível"
              ? () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ProductScreen(product)));
                }
              : null,
          child: type == "grid"
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Hero(
                      tag: "productImage",
                      child: AspectRatio(
                        aspectRatio: 0.8,
                        child: FadeInImage.memoryNetwork(
                          placeholder: kTransparentImage,
                          image: product.images[0],
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              product.title,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              "R\$ ${product.priceMember.toStringAsFixed(2)} Sócios",
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 17.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              "R\$ ${product.price.toStringAsFixed(2)} Não Sócios",
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 14.0,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              : Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Positioned.fill(
                      child: Row(
                        children: <Widget>[
                          Flexible(
                              flex: 1,
                              child: Hero(
                                tag: product.title,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Card(
                                    color: Colors.black87,
                                    margin: EdgeInsets.zero,
                                    clipBehavior: Clip.antiAlias,
                                    borderOnForeground: false,
                                    shape: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          // color: Color.fromARGB(122, 4, 64, 0)),
                                          color: Colors.greenAccent[700]),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                    ),
                                    child: FadeInImage.memoryNetwork(
                                      placeholder: kTransparentImage,
                                      image: product.images[0],
                                      fit: BoxFit.cover,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.24,
                                    ),
                                  ),
                                ),
                              )),
                          Flexible(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    product.title,
                                    style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                  ),
                                  SizedBox(height: 5.0),
                                  RichText(
                                    text: TextSpan(
                                      text:
                                          "R\$ ${product.priceMember.toStringAsFixed(2)} ",
                                      style: TextStyle(
                                        fontFamily: "BebasNeue",
                                        color: Colors.greenAccent[700],
                                        //color: Colors.green[800],
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Sócios",
                                            style: TextStyle(
                                              fontFamily: "BebasNeue",
                                              color: Colors.greenAccent[700]
                                                  .withOpacity(0.54),
                                              /*color: Colors.green[800]
                                                  .withOpacity(0.54),*/
                                              fontSize: 15.0,
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.w600,
                                            ))
                                      ],
                                    ),
                                  ),
                                  /* Text(
                                    "R\$ ${product.priceMember.toStringAsFixed(2)} .:Sócios:.",
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),*/
                                  SizedBox(height: 5.0),
                                  RichText(
                                    text: TextSpan(
                                      text:
                                          "R\$ ${product.price.toStringAsFixed(2)} ",
                                      style: TextStyle(
                                        fontFamily: "BebasNeue",
                                        color: Colors.white70,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Não Sócios",
                                            style: TextStyle(
                                              fontFamily: "BebasNeue",
                                              color: Colors.white54,
                                              fontSize: 13.0,
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.w600,
                                            ))
                                      ],
                                    ),
                                  ),
                                  /*Text(
                                    "R\$ ${product.price.toStringAsFixed(2)} Não Sócios",
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),*/
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: product.status != "Disponível",
                      child: Positioned.fill(
                        child: BackdropFilter(
                          filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                          child: Container(
                            color: Colors.white.withOpacity(0.0),
                          ),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: product.status != "Disponível",
                      child: Positioned.fill(
                          left: 5.0,
                          //top: 10,
                          child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex: 1,
                                  child: Image.asset(
                                    "images/Formiga_pare.png",
                                    fit: BoxFit.contain,
                                    /* height: 180.0,
                                  width:
                                      180.0,*/ // MediaQuery.of(context).size.height,
                                    // width: MediaQuery.of(context).size.width,
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: Text(
                                    "Produto esgotado !",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                    softWrap: true,
                                  ),
                                )
                              ])),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
