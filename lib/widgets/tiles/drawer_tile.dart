import 'package:faen_app_user/models/user_model.dart';
import 'package:faen_app_user/screens/student_card_screen.dart';
import 'package:faen_app_user/screens/user_screen.dart';
import 'package:faen_app_user/tabs/orders_tab.dart';

import 'package:flutter/material.dart';

class DrawerTile extends StatelessWidget {
  final IconData icon;
  final String text;
  final PageController controller;
  final int page;
  final Map<String, dynamic> userData;

  DrawerTile(this.icon, this.text, this.controller, this.page, {this.userData});

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 60.0,
      decoration: BoxDecoration(
        color: controller.page.round() == page
            ? Colors.white60
            : Colors.transparent,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.5),
          bottomLeft: Radius.circular(12.5),
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Navigator.of(context).pop();
            if (page == 4) {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => UserScreen()));
            } else if (page == 5) {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => OrdersTab()));
            } else if (page == 6) {
              UserModel.of(context).userData["typeUser"] != "Não Associado"
                  ? Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => StudentCardScreen()))
                  : showAlertDialog(context);
            }
            // controller.jumpToPage(page);
            /* controller.animateToPage(page,
                duration: Duration(milliseconds: 500), curve: Curves.ease);*/
            /*if (page <= 5) {
              controller.jumpToPage(page);
            } else if (page == 6) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => StudentCardScreen()));
            }*/
          },
          child: Container(
            height: 60.0,
            child: Row(
              children: <Widget>[
                Icon(
                  icon,
                  size: 32.0,
                  color: controller.page.round() == page
                      ? Theme.of(context).primaryColor
                      : Colors.white70,
                ),
                SizedBox(width: 32.0),
                Text(
                  text,
                  style: TextStyle(
                    fontWeight: controller.page.round() == page
                        ? FontWeight.bold
                        : FontWeight.normal,
                    fontSize: 16.0,
                    color: controller.page.round() == page
                        ? Theme.of(context).primaryColor
                        : Colors.white70,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Tudo bem!",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text("Você não tem acesso a Carteirinha!",
                textAlign: TextAlign.center),
            content: Text(
                "Somente usuários Associados FAEN podem acessar a Carteirinha.\n"
                "Realize o pagamento da sua Carteirinha, e/ou entre em contato com um membro Administrador FAEN.",
                textAlign: TextAlign.justify),
            actions: [botaoContinuar]);
      },
    );
  }
}
