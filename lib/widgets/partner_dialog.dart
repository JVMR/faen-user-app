import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PartnerDialog extends StatefulWidget {
  final Map<String, dynamic> partnerData;
  PartnerDialog(this.partnerData);
  @override
  _PartnerDialogState createState() => _PartnerDialogState();
}

class _PartnerDialogState extends State<PartnerDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white.withOpacity(0.85),
      shape: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white10),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: 100,
              width: 100,
              child: Hero(
                tag: widget.partnerData["title"],
                child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    backgroundImage: NetworkImage(widget.partnerData["image"])),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            SelectableText(widget.partnerData["title"],
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center),
            Padding(
              padding: const EdgeInsets.only(right: 80.0, left: 80.0),
              child: Divider(
                height: 18.0,
              ),
            ),
            SelectableText(widget.partnerData["subTitle"],
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                textAlign: TextAlign.center),
            SizedBox(
              height: 8.0,
            ),
            SelectableText(
              widget.partnerData["description"],
              style: TextStyle(
                color: Colors.black,
              ),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: 8.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              //mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                  dense: true,
                  contentPadding: EdgeInsets.all(5.0),
                  leading: Icon(
                    Icons.location_on,
                    size: 33,
                    color: Colors.black,
                  ),
                  title: SelectableText(
                    widget.partnerData["address"],
                    //textWidthBasis: TextWidthBasis.longestLine,
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0),
                    //maxLines: 2,
                    textAlign: TextAlign.justify,
                  ),
                ),
                /*Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,

                  children: <Widget>[
                    Icon(Icons.location_on, size: 33),
                    SelectableText(
                      widget.partnerData["address"],
                      textWidthBasis: TextWidthBasis.longestLine,
                      //style: TextStyle(),
                      //maxLines: 2,
                      textAlign: TextAlign.justify,
                    ),
                  ],
                ),*/
                /* SizedBox(
                  height: 8.0,
                ),*/
                ListTile(
                  dense: true,
                  onTap: () {
                    launch("tel:${widget.partnerData["phone"]}");
                  },
                  contentPadding: EdgeInsets.all(5.0),
                  leading: Icon(
                    Icons.phone,
                    size: 33,
                    color: Colors.black,
                  ),
                  title: SelectableText(
                    widget.partnerData["phone"],
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0),
                  ),
                ),
                /*InkWell(
                  onTap: () {
                    launch("tel:${widget.partnerData["phone"]}");
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.phone, size: 33),
                      SelectableText(widget.partnerData["phone"]),
                    ],
                  ),
                ),*/
              ],
            ),
            Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Voltar",
                      style: TextStyle(color: Colors.greenAccent[700])),
                )),
          ],
        ),
      ),
    );
  }
}
