import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class MarkdownModifiable extends MarkdownWidget {
  final TextOverflow overflow;
  final int maxLines;

  const MarkdownModifiable(
      {Key key,
      String data,
      MarkdownStyleSheet styleSheet,
      SyntaxHighlighter syntaxHighlighter,
      MarkdownTapLinkCallback onTapLink,
      Directory imageDirectory,
      bool selectable,
      this.overflow,
      this.maxLines})
      : super(
          key: key,
          data: data,
          selectable: selectable,
          styleSheet: styleSheet,
          syntaxHighlighter: syntaxHighlighter,
          onTapLink: onTapLink,

          // imageDirectory: imageDirectory,
        );

  @override
  Widget build(BuildContext context, List<Widget> children) {
    var richText = _findWidgetOfType<RichText>(children.first);
    if (richText != null) {
      return RichText(
          text: richText.text,
          //textAlign: richText.textAlign,
          textAlign: TextAlign.justify,
          textDirection: richText.textDirection,
          softWrap: richText.softWrap,
          overflow: this.overflow,
          textScaleFactor: richText.textScaleFactor,
          maxLines: this.maxLines,
          locale: richText.locale);
    }

    return children.first;
  }

  T _findWidgetOfType<T>(Widget widget) {
    if (widget is T) {
      return widget as T;
    }

    if (widget is MultiChildRenderObjectWidget) {
      MultiChildRenderObjectWidget multiChild = widget;
      for (var child in multiChild.children) {
        return _findWidgetOfType<T>(child);
      }
    } else if (widget is SingleChildRenderObjectWidget) {
      SingleChildRenderObjectWidget singleChild = widget;
      return _findWidgetOfType<T>(singleChild.child);
    }

    return null;
  }
}

class MarkdownModifiableSelectableJustify extends MarkdownWidget {
  // final TextOverflow overflow;
  //final int maxLines;

  const MarkdownModifiableSelectableJustify({
    Key key,
    String data,
    MarkdownStyleSheet styleSheet,
    SyntaxHighlighter syntaxHighlighter,
    MarkdownTapLinkCallback onTapLink,
    Directory imageDirectory,
  }) : super(
          key: key,
          data: data,
          selectable:
              true, //Quando false, ele fica justificado, caso contrário não
          styleSheet: styleSheet,
          syntaxHighlighter: syntaxHighlighter,
          onTapLink: onTapLink,
          // imageDirectory: imageDirectory,
        );

  @override
  Widget build(BuildContext context, List<Widget> children) {
    var richText = _findWidgetOfType<RichText>(children.first);
    if (richText != null) {
      return RichText(
          text: richText.text,
          //textAlign: richText.textAlign,
          textAlign: TextAlign.justify,
          textDirection: richText.textDirection,
          //softWrap: richText.softWrap,
          // overflow: this.overflow,
          textScaleFactor: richText.textScaleFactor,
          //maxLines: this.maxLines,
          locale: richText.locale);
    }

    return children.first;
  }

  T _findWidgetOfType<T>(Widget widget) {
    if (widget is T) {
      return widget as T;
    }

    if (widget is MultiChildRenderObjectWidget) {
      MultiChildRenderObjectWidget multiChild = widget;
      for (var child in multiChild.children) {
        return _findWidgetOfType<T>(child);
      }
    } else if (widget is SingleChildRenderObjectWidget) {
      SingleChildRenderObjectWidget singleChild = widget;
      return _findWidgetOfType<T>(singleChild.child);
    }

    return null;
  }
}
