import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/datas/feed_data.dart';
import 'package:faen_app_user/screens/view_post_screen.dart';
import 'package:faen_app_user/widgets/markdown_modifiable.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:transparent_image/transparent_image.dart';

class FeedCard extends StatefulWidget {
  final DocumentSnapshot post;
  final VoidCallback update;

  FeedCard(this.post, this.update);

  @override
  _FeedCardState createState() => _FeedCardState();
}

class _FeedCardState extends State<FeedCard> {
  SharedPreferences _preferences;
  bool _isLiked = false;
  // int _likes = 0;
  bool pressed = false;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance()
      ..then((preferences) {
        setState(() {
          _preferences = preferences;
          _isLiked = _preferences.containsKey(widget.post.data["title"])
              ? _preferences.getBool(widget.post.data["title"])
              : false;
        });
      });

    // if()
    //_isLiked =  ?? false;
    //_likes = widget.post.data["likes"];
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
        stream: Firestore.instance
            .collection("feed")
            .document(widget.post.documentID)
            .snapshots(),
        builder: (context, snapshot) {
          //_likes =
          if (!snapshot.hasData)
            return Card(
              elevation: 8.0,
              //color: Colors.black,
              margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.width * 0.98 / 2,
                      width: MediaQuery.of(context).size.width * 0.98,
                      child: Shimmer.fromColors(
                          child: Container(
                            // color: Colors.white.withAlpha(50),
                            // margin: EdgeInsets.symmetric(vertical: 4),
                            decoration: BoxDecoration(
                              //shape: BoxShape.circle,
                              color: Colors.white.withAlpha(50),
                            ),
                          ),
                          baseColor: Colors.white,
                          highlightColor: Colors.grey),
                    ),
                  ),
                  Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.80,
                              height: 20,
                              child: Shimmer.fromColors(
                                  child: Container(
                                    color: Colors.white.withAlpha(50),
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                  ),
                                  baseColor: Colors.white,
                                  highlightColor: Colors.grey),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.50,
                              height: 18,
                              child: Shimmer.fromColors(
                                  child: Container(
                                    color: Colors.white.withAlpha(50),
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                  ),
                                  baseColor: Colors.white,
                                  highlightColor: Colors.grey),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 17,
                              child: Shimmer.fromColors(
                                  child: Container(
                                    color: Colors.white.withAlpha(50),
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                  ),
                                  baseColor: Colors.white,
                                  highlightColor: Colors.grey),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.90,
                              height: 17,
                              child: Shimmer.fromColors(
                                  child: Container(
                                    color: Colors.white.withAlpha(50),
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                  ),
                                  baseColor: Colors.white,
                                  highlightColor: Colors.grey),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.70,
                              height: 17,
                              child: Shimmer.fromColors(
                                  child: Container(
                                    color: Colors.white.withAlpha(50),
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                  ),
                                  baseColor: Colors.white,
                                  highlightColor: Colors.grey),
                            ),
                          ],
                        ),
                      )),
                ],
              ),
            );
          else
            return Card(
                elevation: 8.0,
                //color: Colors.black.withOpacity(0.8),
                margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: InkWell(
                  onTap: () {
                    FeedData data = FeedData.fromDocument(widget.post);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ViewPostScreen(data)));
                  },
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Flexible(
                          flex: 2,
                          child: widget.post.data["image"] != null
                              ? Hero(
                                  tag: widget.post.data["image"],
                                  child: FadeInImage.memoryNetwork(
                                      placeholder: kTransparentImage,
                                      image: widget.post.data["image"],
                                      fit: BoxFit.cover,
                                      height:
                                          (MediaQuery.of(context).size.width *
                                                  0.98) /
                                              2,
                                      width: MediaQuery.of(context).size.width *
                                          0.98),
                                )
                              : Icon(Icons.error_outline,
                                  color: Colors.red[900], size: 96)),
                      //SizedBox(height: 5.0),
                      Flexible(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                RichText(
                                  text: TextSpan(
                                    text: widget.post.data["title"] + "\n",
                                    style: TextStyle(
                                        fontFamily: "BebasNeue",
                                        //fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontSize: 18.0),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: widget.post.data["subTitle"],
                                          style: TextStyle(
                                              height: 1.4,
                                              //fontWeight: FontWeight.w600,
                                              color: Colors.white70,
                                              fontSize: 16.0)),
                                    ],
                                  ),
                                  textAlign: TextAlign.justify,
                                ),
                                _buildMarkdown(
                                    widget.post.data["description"] ?? "",
                                    context),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                        "Publicado em ${widget.post.data["date"] ?? "um dia ensolarado"}.",
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontStyle: FontStyle.italic)),
                                    FlatButton.icon(
                                        padding: EdgeInsets.zero,
                                        //  splashColor: Colors.greenAccent[700],
                                        onPressed: pressed
                                            ? null
                                            : () {
                                                _toggleVoted(
                                                    snapshot.data["likes"]);
                                              },
                                        icon: Icon(Icons.thumb_up,
                                            color: _isLiked
                                                ? Colors.greenAccent[700]
                                                : Colors.white70),
                                        label: Text(
                                            "${snapshot.data["likes"] ?? 0}",
                                            style: TextStyle(
                                                color: _isLiked
                                                    ? Colors.greenAccent[700]
                                                    : Colors.white70))),
                                    /*Text("$_likes",
                                        style: TextStyle(color: Colors.white70))),*/
                                  ],
                                ),
                              ],
                            ),
                          )),
                    ],
                  ),
                ));
        });
  }

  Widget _buildMarkdown(String data, BuildContext context) {
    return MarkdownModifiable(
      // shrinkWrap: true,
      //selectable: true,
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
      data: data,
      selectable: false,
      styleSheet: MarkdownStyleSheet(
        checkbox: TextStyle(color: Colors.white),
        h1Align: WrapAlignment.center,
        textAlign: WrapAlignment.spaceBetween,
        p: Theme.of(context).textTheme.bodyText1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h1: Theme.of(context).textTheme.headline1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h2: Theme.of(context).textTheme.headline2.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h3: Theme.of(context).textTheme.headline3.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h4: Theme.of(context).textTheme.headline4.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h5: Theme.of(context).textTheme.headline5.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h6: Theme.of(context).textTheme.headline6.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
      ),
    );
  }

  void _toggleVoted(int likes) async {
    int deltaVotes = 0;
    setState(() {
      pressed = true;
      _isLiked = !_isLiked;
      deltaVotes = _isLiked ? 1 : -1;
    });
    try {
      await _preferences.setBool(widget.post.data["title"], _isLiked);
      await widget.post.reference.updateData({"likes": likes + deltaVotes});
      //_likes += deltaVotes;
    } catch (e) {
      print("Erro ao salvar: " + e.toString());
    }
    widget.update();

    setState(() {
      pressed = false;
    });
  }
}
