import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_user/models/cart_model.dart';
import 'package:flutter/material.dart';

class DiscountCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: ExpansionTile(
        leading: Icon(Icons.card_giftcard, color: Colors.white),
        title: Text(
          "Cupom de Desconto",
          textAlign: TextAlign.start,
          style: TextStyle(fontWeight: FontWeight.w500, color: Colors.white),
        ),
        trailing: Icon(Icons.add, color: Colors.white),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white60,
                    ),
                    borderRadius: BorderRadius.circular(12.5)),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.green[900],
                    ),
                    borderRadius: BorderRadius.circular(12.5)),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red[900],
                    ),
                    borderRadius: BorderRadius.circular(12.5)),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red[900],
                    ),
                    borderRadius: BorderRadius.circular(12.5)),
                hintText: "Digite seu cupom",
                hintStyle: TextStyle(color: Colors.white70),
              ),
              initialValue: CartModel.of(context).couponCode ?? "",
              onFieldSubmitted: (text) {
                Firestore.instance
                    .collection("coupons")
                    .document(text)
                    .get()
                    .then((docSnap) {
                  if (docSnap.data != null) {
                    CartModel.of(context)
                        .setCoupon(text, docSnap.data["percent"]);
                    Scaffold.of(context).showSnackBar(SnackBar(
                        content: Text(
                            "Desconto de ${docSnap.data["percent"]}% aplicado"),
                        backgroundColor: Colors.greenAccent[700]));
                  } else {
                    CartModel.of(context).setCoupon(null, 0);
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text("Cupom não existente!"),
                      backgroundColor: Colors.red[900],
                    ));
                  }
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
