import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

/*export const sendToDevice = functions.firestore
    .document('coupons/{couponsId}').onWrite(async snapshot => {


        //const order = snapshot.data();

         const querySnapshot = await db
              .collection('users')
              .doc('GjDCyQG37mSZ1JX3nJOMXbIckuF3')
              .collection('tokens')
              .get();

        // const tokens = querySnapshot.docs.map(snap => snap.id);

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: 'Graças a Deus!',
                body: 'Deu certo para honra e glória do nosso Senhor !',
                icon: '@mipmap/ic_launcher',
                click_action: 'FLUTTER_NOTIFICATION_CLICK'
            }
        };

        return fcm.sendToDevice("dnxpfrbbUkQ:APA91bHHzi_4gLTX396zE2clj6BWHHOjo3lzYR13-F1Dq-9xauPgkc0WFjcES6TkTcbFiGYbEFMmvJQ8naO-oYmGKJDjnn3ycuIX5OjKuvDeecUUu7JAA279Xb73nHRZ8Mm8SHGKXZfw", payload);
    });*/

/*export const sendToTopic = functions.firestore
    .document('coupons/{couponsId}')
    .onCreate(async snapshot => {
        //const coupons = snapshot.data();

        //const alo = coupons.title == null 0? "alo";

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: 'Otimismo !',
                body: `Deus é bom, não é que funcionou`,
                icon: '@mipmap/ic_launcher',
                click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
            }
        };
        //return fcm.sendAll(payload);
        return fcm.sendToTopic('user', payload);
    });*/

export const sendNewFeedPost = functions.firestore
    .document('feed/{feedId}')
    .onCreate(async snapshot => {
        const post = snapshot.data();

        // const title = post?.title ?? "Título";

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: `Novo post no feed ! :)`,
                image: `${post?.image}`,
                body: `${post?.title} - ${post?.subTitle}`,
                icon: '@mipmap/ic_launcher',
                click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
            },
            data: {
                type: 'newPost'
            }

        };

        return fcm.sendToTopic('members', payload);
    });

export const updateOrder = functions.firestore
    .document('orders/{orderId}')
    .onUpdate(async snapshot => {


        const order = snapshot.after.data();

        const querySnapshot = await db
            .collection('users')
            .doc(order?.clientId)
            .collection('tokens')
            .get();

        const tokens = querySnapshot.docs.map(snap => snap.id);

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: 'O status do seu foi pedido atualizado !',
                body: `Status atual: ${order?.status.toString()} de 4`,
                icon: '@mipmap/ic_launcher',
                click_action: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                type: "orderUpdate",
            }
        };

        return fcm.sendToDevice(tokens, payload);
    });

export const sendNewPartner = functions.firestore
    .document('partners/{partnerId}')
    .onCreate(async snapshot => {
        const partner = snapshot.data();

        // const title = post?.title ?? "Título";

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: `Alô formigueiro, tem Nova Parceria fechada !`,
                image: `${partner?.image}`,
                body: `${partner?.title} - ${partner?.subTitle}\nAcesse o app e saiba mais.`,
                icon: '@mipmap/ic_launcher',
                click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
            },
            data: {
                type: 'newPartner'
            }

        };

        return fcm.sendToTopic('members', payload);
    });

export const sendNewOrderToAdms = functions.firestore
    .document('orders/{orderId}')
    .onCreate(async snapshot => {
        const order = snapshot.data();

        // const title = post?.title ?? "Título";

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: `Novo pedido realizado !`,
                body: `${order?.title} - ${order?.subTitle}\nAcesse o app e saiba mais.`,
                icon: '@mipmap/ic_launcher',
                click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
            },
            data: {
                type: 'newOrder'
            }

        };

        return fcm.sendToTopic('test', payload);
    });


export const deleteUser = functions.firestore
    .document('users/{userID}')
    .onDelete(async snap => {
        return admin.auth().deleteUser(snap.id)
            .then(() => console.log('Deleted user with ID:' + snap.id))
            .catch((error) => console.error('There was an error while deleting user:', error));
    });


// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
